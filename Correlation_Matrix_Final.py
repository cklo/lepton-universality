﻿import numpy as np
import ROOT
import array
import math
import pandas as pd
import seaborn as sn
import matplotlib.pyplot as plt

#DataFile = ROOT.TFile.Open["/afs/cern.ch/user/c/cklo/public/TestingData.root"]
rootfile = "/afs/cern.ch/user/c/cklo/public/TestingData_1.root"
DataFile = ROOT.TFile(rootfile)
ROOT.gDirectory.pwd()
tree = ROOT.gDirectory.Get("tree")
attr_names = ["BmumuKst_B_mass",
              "BmumuKst_CosTheta",
              "BmumuKst_CosTheta_err",
              "BmumuKst_B_tau_constM_PVMinA0",
              "BmumuKst_B_tau_over_err",
              "BmumuKst_Lxy_minA0",
              "BmumuKst_Lxy_over_err"
              ]
n_events = tree.GetEntries()
print(n_events)
LC = 4700
passedData_CosTheta = []
passedData_CosTheta_err = []
passedData_B_tau_constM_PVMinA0 = []
passedData_B_tau_over_err = []
passedData_Lxy_minA0 = []
passedData_Lxy_over_err = []
for i in range(n_events):  # n_events): #n_events
    tree.GetEntry(i)  # access event i
    mass = getattr(tree, attr_names[0])
    BmumuKst_CosTheta = getattr(tree, attr_names[1])
    BmumuKst_CosTheta_err = getattr(tree, attr_names[2])
    BmumuKst_B_tau_constM_PVMinA0 = getattr(tree, attr_names[3])
    BmumuKst_B_tau_over_err = getattr(tree, attr_names[4])
    BmumuKst_Lxy_minA0 = getattr(tree, attr_names[5])
    BmumuKst_Lxy_over_err = getattr(tree, attr_names[6])
    if LC < mass < 5800:
        passedData_CosTheta.append(BmumuKst_CosTheta)  # Fill B_mass to your histogram h2
        passedData_CosTheta_err.append(BmumuKst_CosTheta_err)  # Fill B_mass to your histogram h2
        passedData_B_tau_constM_PVMinA0.append(BmumuKst_B_tau_constM_PVMinA0)  # Fill B_mass to your histogram h2
        passedData_B_tau_over_err.append(BmumuKst_B_tau_over_err)  # Fill B_mass to your histogram h2
        passedData_Lxy_minA0.append(BmumuKst_Lxy_minA0)  # Fill B_mass to your histogram h2
        passedData_Lxy_over_err.append(BmumuKst_Lxy_over_err)  # Fill B_mass to your histogram h2

List = [passedData_CosTheta,passedData_CosTheta_err,passedData_B_tau_constM_PVMinA0,passedData_B_tau_over_err,passedData_Lxy_minA0, passedData_Lxy_over_err]
Num_Entry = len(passedData_CosTheta)
Extracted_Element_List = [[] for i in range(Num_Entry)]
for i in range(Num_Entry):
    Extracted_Element_List[i] = [item[i] for item in List]
data = pd.DataFrame(Extracted_Element_List, columns=["Cos","Cos_err","tau","tau_err","Lxy","Lxy_err"])
sn.heatmap(data.corr(method='pearson'),linewidths=0.1,vmax=1.0, square=True, linecolor='White', annot=True)
plt.savefig("correlationmatrix.png")