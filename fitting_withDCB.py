import numpy as np
import ROOT
from array import array

#-----------------------Accessing Root files---------------------------------
"""file1 = ROOT.TFile.Open("/afs/cern.ch/user/n/nheatley/B_masses_cutflow_Jpsi.root")  # or B_masses_cutflow_Psi2s.root"
ROOT.gDirectory.pwd()
tree1 = ROOT.gDirectory.Get("tree_NR") #or "tree_NR"

file2 = ROOT.TFile.Open("/afs/cern.ch/user/n/nheatley/cutflow/MCJPsi_JPsireg.root")
ROOT.gDirectory.pwd()
tree = ROOT.gDirectory.Get("tree")
"""	
def createCanvasPads():
    c1 =  ROOT.TCanvas('B_Vertex_Mass','Events', 800, 800)
    # Upper histogram plot is pad1
    pad1 = ROOT.TPad("pad1", "pad1", 0, 0.3, 1, 1.0) #xmin, ymin, xmax, ymax
    pad1.SetBottomMargin(0)  # joins upper and lower plot
    pad1.SetGridx()
    pad1.Draw()
    # Lower ratio plot is pad2
    c1.cd()  # returns to main canvas before defining pad2
    pad2 = ROOT.TPad("B_Vertex_Mass", "B_Vertex_Mass", 0, 0, 1, 0.3)
    pad2.SetTopMargin(0)  # joins upper and lower plot
    pad2.SetBottomMargin(0.2)
    pad2.SetGridx()
    pad2.Draw()

    return c1, pad1, pad2
#c1, pad1, pad2 = createCanvasPads()

def DataPlotter(ROOTfile,q2reg):
	#-------------------------defining ROOT variables--------------------
	file1 = ROOT.TFile.Open(ROOTfile) #open 1st file
    	ROOT.gDirectory.pwd() #work in the open file
        tree= ROOT.gDirectory.Get("tree")

	attr_names = ["BmumuKst_B_mass","BmumuKst_muon0_eta","BmumuKst_muon1_eta", "BmumuKst_meson0_eta","BmumuKst_meson1_eta","BmumuKst_diMuon_mass"]
	
	mass         = np.zeros(1,dtype=float)
	numEntry     = tree.GetEntries()
	w = ROOT.RooWorkspace("w")
	m = ROOT.RooRealVar('m', 'm', 4700., 5700.)
	m_arg = ROOT.RooArgSet(m)
	w.Import(m_arg)
	data = ROOT.RooDataSet("data", "data", m_arg)

	data.Print("v")
	#-------------------------Accessing data-----------------
	n_events = tree.GetEntries()
	print(n_events)
	for i in range(n_events): #n_events
		tree.GetEntry(i) #access event i
		mass =  getattr(tree,attr_names[0])
		#tau = getattr(tree, "BmumuKst_B_tau_constM_PVMinA0")
		#if tau > 0.20:
		if 4700 < mass < 5700:
			ROOT.RooAbsRealLValue.__assign__(m, mass)
			data.add(m_arg,1.0)
	data.Print("v")
	filename = "TestingFitting"
	#-----------------Fitting---------------------
	m = w.var('m')
	m.setRange("Overall", 4700, 5700.)

	#w.factory("RooCBShape::signal1(m, mu[5280,5200,5300],  sig1[30,70], alpha1[0,10], n1[0,10])") #, alphaR[2,0,10], nR[5,0,500])")
	#w.factory("Gaussian:signal2(m, mu, sig2[202.72,150,350])")
	ROOT.gROOT.ProcessLineSync(".x Double_Crystal_Ball.cxx+")
	w.factory("Double_Crystal_Ball:signal_pdf(m, mu[5280, 5200, 5300], 55.5341, 1.15248*2*0.9675, 3.3042*0.9900, 1.08479*0.975, 6.78577*0.9984)")
	#w.factory("Gaussian:signal_pdf(m, mu1[5280, 5200, 5300], sig2[76.3703,60,90])")
	#w.factory("SUM:signal_model(No_sig1[4042.05,4000,5000]*DCB_pdf, No_sig2[1155.18,1000,2000]*signal_pdf)")
	#w.factory("RooPolynomial:bkg_pdf(m, c1[-0.1, 0], 1)") #c1[-35e-5, -1e-2, 1e-6
	#w.factory("Gaussian:signal_pdf(m, mu[5280, 5200, 5300], sig2[202.72,150,350])")
	#w.factory("RooExponential:bkg_pdf1(m, c2[-0.0027, -0.1,0])")
	w.factory("RooExponential:bkg_pdf(m, c1[-1e-4, -1e-2, 1e-6])")
	w.factory("RooExponential:bkg_pdf1(m, c2[-35e-5, -1e-2, 1e-6])")
	w.factory("RooExponential:bkg_pdf2(m, c3[-2e-4, -1e-2, 1e-6])")
	#w.factory("Exponential:bkg_model(m, p1[-0.0015,-0.0016, -0.0014], p2[10,9.99,10.01])")
	w.factory("SUM:bkg_model(No_sig3[200,0,5000]*bkg_pdf, No_sig4[200,0,5000]*bkg_pdf1, No_sig5[200,0,5000]*bkg_pdf2)")
	#w.factory("RSUM:signal_pdf(f1[0.8050,1]*signal1,signal2)")
	#w.factory("SUM:model(No_sig[2000,0,50000]*signal_model, No_bck[40000,0,60000]*bkg_model)")
	#w.factory("SUM:model(No_sig[6000,5999,6001]*signal_pdf, No_bck[20000,19999,20001]*bkg_model)")
	w.factory("SUM:model(No_sig[2000,0,50000]*signal_pdf, No_bck[40000,0,60000]*bkg_model)")

	pdf = w.pdf('model')
	#pdf = w.pdf('model')
	"""
	#w.factory("Gaussian:signal2(m, mu, sigma2[140,0,500])")
	w.factory("RooCBShape::signal1(m, mu[5262.1,5200,5300],  sig1[89.3,80,90], alpha1[0.41,0.01,10], n1[1.6,0.01,10])") #, alphaR[2,0,10], nR[5,0,500])")
	w.factory("Gaussian:signal2(m, mu, sig2[258,0,300])")
	#w.factory("Gaussian:signal3(m, mu, sig3[25, 0, 50])")
	#w.factory("RooCBShape::signal2(m, mu, sigma2[140,0,500], alpha2[1.5,0,100], n2[2.5,0,500])")

	w.factory("RooExponential:bkg_pdf(m, c1[-0.,0,0.])")
	#.factory("Roo[])")
	#w.factory("RooExponential:bkg2(m, c2[-0.004593,-6e-3,-1e-3])")
	#w.factory("RSUM:bkg_pdf(f2[0.5,0.0,1]*bkg1,bkg2)")
	#w.factory("Gaussian::bkg(m, mu, sigma3[500,0,1000])")

	w.factory("RSUM:signal_pdf(f1[0.835,0,1]*signal1,signal2)")
	#w.factory("RSUM:signal_pdf(f2[0.172]*signal_t,signal3)")
	#.factory("RSUM:bkg_pdf(f2[0.5,0,1]*bkg1, bkg2)")  No_sig[9453,0 ,45000]

	w.factory("SUM:model(No_sig[30000,0,200000]*signal_pdf, No_bck[0,0,1000]*bkg_pdf)")
	"""
	#pdf = w.pdf('model')
	pdf.fitTo(data, ROOT.RooFit.PrintLevel(-1), ROOT.RooFit.Range("Overall")) #, ROOT.RooFit.Range("4700,5700"))
	#------------------Create pads for plots--------------
	#------------------plotting--------------------
	c1, pad1, pad2 = createCanvasPads()
	pad1.cd()  #upper pad
	pl = m.frame(ROOT.RooFit.Title("B_Meson_Mass"))
	data.plotOn(pl)
	#pdf.plotOn(pl, ROOT.RooFit.Components("signal_model"), ROOT.RooFit.LineStyle(2))
	pdf.plotOn(pl, ROOT.RooFit.Components("signal_pdf"), ROOT.RooFit.LineColor(1), ROOT.RooFit.LineStyle(2))
	#pdf.plotOn(pl, ROOT.RooFit.Components("signal2"), ROOT.RooFit.LineStyle(2))
	pdf.plotOn(pl, ROOT.RooFit.Components("bkg_model"), ROOT.RooFit.LineColor(2), ROOT.RooFit.LineStyle(2))
	pdf.plotOn(pl, ROOT.RooFit.Components("model"), ROOT.RooFit.LineColor(3), ROOT.RooFit.LineStyle(2))

	pdf.paramOn(pl, ROOT.RooFit.Layout(0.7,0.9,0.9))
	pdf.plotOn(pl)
	chi_square = pl.chiSquare(9) # Number of free parameter in your fitting
	print(chi_square)
	legend = ROOT.TLegend(0.15, 0.15, 0.25, 0.25) #Position (xmin,ymin,xmax,ymax)
	legend.SetBorderSize(0)  # no border
	legend.SetFillStyle(0)  # transparent
	legend.SetTextSize(.05)
	legend.AddEntry(pl,	'#chi^{2}' + ' / ndf = {:.3f}'.format(chi_square), '')
	pl.GetXaxis().SetRangeUser(4700., 5700.)
	#pl.GetYaxis().SetRangeUser(0., 1500.)
	pl.SetMinimum(0.01)
	#pl.GetYaxis().SetRangeUser(0.01, 1000.)
	pl.SetTitle("")
	pl.Draw()  #draw on the current pad
	legend.Draw()
	pad2.cd()  #lowerpad

	pullhist = pl.pullHist()
	pl2 = m.frame(ROOT.RooFit.Title(""))
	pl2.addPlotable(pullhist, "P")

	pl2.GetXaxis().SetNdivisions(207)
	pl2.GetYaxis().SetNdivisions(207)
	pl2.GetYaxis().SetLabelSize(0.07)

	pl2.GetYaxis().SetRangeUser(-5., 5.)
	pl2.GetXaxis().SetTitleSize(0.10)
	pl2.GetXaxis().SetLabelSize(0.07)
	#residhist.SetMaximum(9.99)

	pl2.SetTitle("")
	pl2.GetXaxis().SetTitle("B_Vertex_Mass [MeV]")
	pl2.GetYaxis().SetTitle("Pull")
	pl2.GetYaxis().SetTitleSize(0.09)
	pl2.GetYaxis().SetTitleOffset(0.3)

	pl2.Draw()

	line = ROOT.TLine(4700.,0,5700.,0)
	line.SetLineColor(ROOT.kRed)
	line.SetLineWidth(2)
	line.Draw("same")

	c1.Draw()
	c1.SaveAs("{}.png".format(filename))

#file = "/afs/cern.ch/user/c/cklo/public/TestingData_2.root"
file = "/afs/cern.ch/user/c/cklo/public/TestingData_PostCRS_5.root"
#Psi2S_MC_Highq2_PS = "/afs/cern.ch/user/n/nheatley/cutflow/MCPsi2S_Psi2Sreg_bestcandtight.root"
#Jpsi_MC_Highq2_PS = "../cutflow/MCJPsi_JPsireg_bestcandtight_NoTauCut.root"
#Jpsi_Data_Highq2_PS = "../cutflow/data_JPsireg_bestcandtight_Merge.root"
DataPlotter(file,"Highq2")

