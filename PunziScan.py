import pandas
import ROOT
from ROOT import RooFit
from array import array
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
matplotlib.rcParams['text.usetex']=True
matplotlib.rcParams['text.latex.unicode']=True


def PunziSignifScan(MCfile, Datafile, attr,bkgFirst,LC, HC, cuts, optimise=False):
    TFileMC   = ROOT.TFile(MCfile)
    TTreeMC   = TFileMC.Get("tree")
    TFileData = ROOT.TFile(Datafile)
    TTreeData = TFileData.Get("tree")

    passedMCs = []
    passedDatas = []

    nData  = 100000#TTreeData.GetEntries()
    nMC    = 100000#TTreeMC.GetEntries()
    cutlist = np.linspace(LC,HC,cuts)
    reg1, reg2, reg3 = (0.19747546044624495, 0.6406002800264298, 0.16192425952732517) # From SidebandFit1
    SB_factor = reg2/(reg1+reg3)
    for cut in cutlist: 
        passedMC = 0
        passedData = 0
        if bkgFirst == True:
            for event in range(nData):
                TTreeData.GetEntry(event)
                if getattr(TTreeData,attr) > cut:
                    passedData += 1
            for event in range(nMC):
                TTreeMC.GetEntry(event)
                truthID = getattr(TTreeMC, "BmumuKst_isTrueResBd")
                if getattr(TTreeMC, attr) > cut and truthID == 1:
                    passedMC += 1
            passedMCs.append(passedMC)
            passedDatas.append(passedData)
        else:
            for event in range(nData):
                TTreeData.GetEntry(event)
                if getattr(TTreeData,attr) < cut:
                    passedData += 1
            for event in range(nMC):
                TTreeMC.GetEntry(event)
                truthID = getattr(TTreeMC, "BmumuKst_isTrueResBd")
                if getattr(TTreeMC, attr) < cut  and truthID == 1:
                    passedMC += 1
            passedMCs.append(passedMC)
            passedDatas.append(passedData)

    a = 2.
    eps_cuts = np.array(passedMCs)/float(nMC)
    
    Punzi_sigs = eps_cuts/(a/2 + np.sqrt(SB_factor*np.array(passedDatas)))
    print("highest signif of",max(Punzi_sigs),"at", cutlist[np.argmax(Punzi_sigs)])
    if optimise == False:
        return cutlist, Punzi_sigs
    else:
        return passedMCs, passedDatas

coscuts, cossigs = PunziSignifScan("/afs/cern.ch/user/c/cklo/public/TestingMC.root","/afs/cern.ch/user/c/cklo/public/TestingData_Sideband_JPsi.root","BmumuKst_CosTheta",True,0.999,1,50)
cossigcuts, cossigsigs = PunziSignifScan("/afs/cern.ch/user/c/cklo/public/TestingMC.root","/afs/cern.ch/user/c/cklo/public/TestingData_Sideband_JPsi.root","BmumuKst_CosTheta_err",True,0,0.5,100)
taucuts, tausigs = PunziSignifScan("/afs/cern.ch/user/c/cklo/public/TestingMC.root","/afs/cern.ch/user/c/cklo/public/TestingData_Sideband_JPsi.root","BmumuKst_B_tau_constM_PVMinA0",True,2,0,60)
tausigcuts, tausigsigs = PunziSignifScan("/afs/cern.ch/user/c/cklo/public/TestingMC.root","/afs/cern.ch/user/c/cklo/public/TestingData_Sideband_JPsi.root","BmumuKst_B_tau_over_err",True,60,0,60)
Lxycuts, Lxysigs = PunziSignifScan("/afs/cern.ch/user/c/cklo/public/TestingMC.root","/afs/cern.ch/user/c/cklo/public/TestingData_Sideband_JPsi.root","BmumuKst_Lxy_minA0",True,3,0,60)
Lxysigcuts, Lxysigsigs = PunziSignifScan("/afs/cern.ch/user/c/cklo/public/TestingMC.root","/afs/cern.ch/user/c/cklo/public/TestingData_Sideband_JPsi.root","BmumuKst_Lxy_over_err",True,60,0,60)
       
plt.figure(1)
fig, axs = plt.subplots(3,2)
axs[0,0].plot(coscuts,cossigs,label='NR Sideband')
axs[0,0].set(xlabel =r'$\cos{\theta}$',ylabel=r'$\epsilon_{cut}/(a/2 + \sqrt{B})$')
#axs[0,0].set_ylim([200,265])
#axs[0].legend(fontsize=10)

axs[0,1].plot(cossigcuts, cossigsigs,label='NR Sideband')
axs[0,1].set(xlabel =r'$\cos{\theta}/\sigma{_{\cos{\theta}}}$ cut',ylabel=r'$\epsilon_{cut}/(a/2 + \sqrt{B})$')
#axs[0,1].set_ylim([195,245])
#axs[0,1].legend(fontsize=10)

axs[1,0].plot(taucuts, tausigs,label='NR Sideband')
axs[1,0].set(xlabel =r'$\tau$',ylabel=r'$\epsilon_{cut}/(a/2 + \sqrt{B})$')
#axs[0,0].set_ylim([200,265])
#axs[1,0].legend(fontsize=10)

axs[1,1].plot(tausigcuts, tausigsigs,label='NR Sideband')
axs[1,1].set(xlabel =r'$\tau/\sigma_{\tau}$ cut',ylabel=r'$\epsilon_{cut}/(a/2 + \sqrt{B})$')
#axs[0,1].set_ylim([195,245])
#axs[1,1].legend(fontsize=10)

axs[2,0].plot(Lxycuts, Lxysigs,label='NR Sideband')
axs[2,0].set(xlabel =r'$L_{xy}$',ylabel=r'$\epsilon_{cut}/(a/2 + \sqrt{B})$')
#axs[2,0].legend(fontsize=10)

axs[2,1].plot(Lxysigcuts, Lxysigsigs,label='NR Sideband')
axs[2,1].set(xlabel =r'$L_{xy}/\sigma_{L}$',ylabel=r'$\epsilon_{cut}/(a/2 + \sqrt{B})$')
#axs[1,1].set_ylim([200,245])
#axs[2,1].legend(fontsize=10)

plt.savefig("TestingPunziScan.png")
