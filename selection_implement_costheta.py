import numpy as np
import ROOT
from array import array
#import matplotlib.pyplot as plt
import pandas as pd
import csv
import ml_config
import math

def cos_Theta_error(a, a_err, b, b_err):   # Calculate cos_theta error, where theta is the angle between vector a and b
    ab = a.Dot(b)
    a2 = a.Dot(a)
    b2 = b.Dot(b)
    ab2 = a2 * b2
    ab3 = math.pow(ab2, 3/2)

    dax2_axerr2 = math.pow( ( b.X() * ab2 - a.X() * b2 * ab ) * a_err.X() / ab3 , 2)
    day2_ayerr2 = math.pow( ( b.Y() * ab2 - a.Y() * b2 * ab ) * a_err.Y() / ab3 , 2)
    daz2_azerr2 = math.pow( ( b.Z() * ab2 - a.Z() * b2 * ab ) * a_err.Z() / ab3 , 2)

    dbx2_bxerr2 = math.pow( ( a.X() * ab2 - b.X() * a2 * ab ) * b_err.X() / ab3 , 2)
    dby2_byerr2 = math.pow( ( a.Y() * ab2 - b.Y() * a2 * ab ) * b_err.Y() / ab3 , 2)
    dbz2_bzerr2 = math.pow( ( a.Z() * ab2 - b.Z() * a2 * ab ) * b_err.Z() / ab3 , 2)


    return math.sqrt( dax2_axerr2 + day2_ayerr2 + daz2_azerr2 + dbx2_bxerr2 + dby2_byerr2 + dbz2_bzerr2 )

"""
MCNR_file = ROOT.TFile("/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08/ntuple-300590_part_01.root")
MC_NR_tree_file = MCNR_file.Get("Nominal/BaseSelection_KStaree_BeeKstSelection")
n_events = MC_NR_tree_file.GetEntries()

BeeKst_CosTheta_hist = ROOT.TH1D("BeeKst_CosTheta","BeeKst_CosTheta",40,-1,1)
BeeKst_CosTheta_err_hist= ROOT.TH1D("BeeKst_CosTheta_err","BeeKst_CosTheta_err",40,0,1000)

print(n_events)
for i in range (10000):
	if i%1000 == 0:
		print(i)
	MC_NR_tree_file.GetEntry(i)
	m_B = getattr(MC_NR_tree_file, "BeeKst_B_mass")
	vtx_BeeKst_x = getattr(MC_NR_tree_file, "BeeKst_x")
	vtx_BeeKst_y = getattr(MC_NR_tree_file, "BeeKst_y")
	vtx_BeeKst_z = getattr(MC_NR_tree_file, "BeeKst_z")
	vtx_BeeKst_PV_minA0_x = getattr(MC_NR_tree_file, "BeeKst_PV_minA0_x")
	vtx_BeeKst_PV_minA0_y = getattr(MC_NR_tree_file, "BeeKst_PV_minA0_y")
	vtx_BeeKst_PV_minA0_z = getattr(MC_NR_tree_file, "BeeKst_PV_minA0_z")
	
	e0_pt   =  getattr(MC_NR_tree_file,"BeeKst_electron0_pT")
	e0_eta  =  getattr(MC_NR_tree_file,"BeeKst_electron0_eta")
	e0_phi  =  getattr(MC_NR_tree_file,"BeeKst_electron0_phi")
        e1_pt   =  getattr(MC_NR_tree_file,"BeeKst_electron1_pT")
        e1_eta  =  getattr(MC_NR_tree_file,"BeeKst_electron1_eta")
        e1_phi  =  getattr(MC_NR_tree_file,"BeeKst_electron1_phi")

	mes0_pt   =  getattr(MC_NR_tree_file, "BeeKst_meson0_pT")
	mes1_pt   =  getattr(MC_NR_tree_file, "BeeKst_meson1_pT")
	mes0_eta  =  getattr(MC_NR_tree_file, "BeeKst_meson0_eta")
	mes1_eta  =  getattr(MC_NR_tree_file, "BeeKst_meson1_eta")
	mes0_phi  =  getattr(MC_NR_tree_file, "BeeKst_meson0_phi")
	mes1_phi  =  getattr(MC_NR_tree_file, "BeeKst_meson1_phi")
	
	vtx_BeeKst_PV_minA0_x_err = getattr(MC_NR_tree_file, "BeeKst_PV_minA0_x_err")
	vtx_BeeKst_PV_minA0_y_err = getattr(MC_NR_tree_file, "BeeKst_PV_minA0_y_err")
	vtx_BeeKst_PV_minA0_z_err = getattr(MC_NR_tree_file, "BeeKst_PV_minA0_z_err")
	vtx_BeeKst_x_err          = getattr(MC_NR_tree_file, "BeeKst_x_err")
	vtx_BeeKst_y_err          = getattr(MC_NR_tree_file, "BeeKst_y_err")
	vtx_BeeKst_z_err          = getattr(MC_NR_tree_file, "BeeKst_z_err")

	for ind, var in enumerate(m_B):
		vtx_Bd_vtx = ROOT.TVector3()
		vtx_Bd_vtx.SetXYZ(vtx_BeeKst_x[ind], vtx_BeeKst_y[ind], vtx_BeeKst_z[ind])
		vtx_Bd_pv  = ROOT.TVector3()
		vtx_Bd_pv.SetXYZ(vtx_BeeKst_PV_minA0_x[ind], vtx_BeeKst_PV_minA0_y[ind], vtx_BeeKst_PV_minA0_z[ind])
		vtx_pv2B   = vtx_Bd_vtx - vtx_Bd_pv
	
                vtx_e0p4 = ROOT.TLorentzVector()
		vtx_e0p4.SetPtEtaPhiM(e0_pt[ind], e0_eta[ind], e0_phi[ind], ml_config.PDG["e_mass"])
                vtx_e1p4 = ROOT.TLorentzVector()
                vtx_e1p4.SetPtEtaPhiM(e1_pt[ind], e1_eta[ind], e1_phi[ind], ml_config.PDG["e_mass"])
		vtx_diElectron_p4 = vtx_e0p4 + vtx_e1p4

		vtx_m0_pi_p4 = ROOT.TLorentzVector()
		vtx_m0_pi_p4.SetPtEtaPhiM(mes0_pt[ind], mes0_eta[ind], mes0_phi[ind], ml_config.PDG["pipm_mass"])
		vtx_m0_K_p4  = ROOT.TLorentzVector()
		vtx_m0_K_p4.SetPtEtaPhiM(mes0_pt[ind], mes0_eta[ind], mes0_phi[ind],  ml_config.PDG["Kpm_mass"])
		vtx_m1_pi_p4 = ROOT.TLorentzVector()
                vtx_m1_pi_p4.SetPtEtaPhiM(mes1_pt[ind], mes1_eta[ind], mes1_phi[ind], ml_config.PDG["pipm_mass"])
		vtx_m1_K_p4  = ROOT.TLorentzVector()
		vtx_m1_K_p4.SetPtEtaPhiM(mes1_pt[ind], mes1_eta[ind], mes1_phi[ind],  ml_config.PDG["Kpm_mass"])

		vtx_Kpi_p4        = vtx_m0_K_p4 + vtx_m1_pi_p4
		vtx_piK_p4        = vtx_m0_pi_p4 + vtx_m1_K_p4
		vtx_Bd_p4         = vtx_diElectron_p4 + vtx_Kpi_p4
		vtx_Bdbar_p4      = vtx_diElectron_p4 + vtx_piK_p4

		vtx_Bd_p3 = vtx_Bd_p4.Vect()

		vtx_Bd_p3_err   = ROOT.TVector3()
		vtx_Bd_p3_err.SetXYZ(0.,0.,0.)
		
		vtx_Bd_pv_err   = ROOT.TVector3()
		vtx_Bd_pv_err.SetXYZ(vtx_BeeKst_PV_minA0_x_err[ind], vtx_BeeKst_PV_minA0_y_err[ind], vtx_BeeKst_PV_minA0_z_err[ind])		
		
		vtx_Bd_vtx_err  = ROOT.TVector3()
		vtx_Bd_vtx_err.SetXYZ(vtx_BeeKst_x_err[ind],vtx_BeeKst_y_err[ind],vtx_BeeKst_z_err[ind])

		vtx_pv2B_err    = ROOT.TVector3()
		vtx_pv2B_err.SetXYZ(np.sqrt(vtx_Bd_vtx_err.X() * vtx_Bd_vtx_err.X() + vtx_Bd_pv_err.X() * vtx_Bd_pv_err.X()), np.sqrt( vtx_Bd_vtx_err.Y() * vtx_Bd_vtx_err.Y() + vtx_Bd_pv_err.Y() * vtx_Bd_pv_err.Y()), np.sqrt( vtx_Bd_vtx_err.Z() * vtx_Bd_vtx_err.Z() + vtx_Bd_pv_err.Z() * vtx_Bd_pv_err.Z()) )


		BeeKst_CosTheta  = vtx_pv2B.Dot(vtx_Bd_p3)/np.sqrt(vtx_pv2B.Dot(vtx_pv2B) * vtx_Bd_p3.Dot(vtx_Bd_p3))
		BeeKst_CosTheta_hist.Fill(BeeKst_CosTheta)
		BeeKst_CosTheta_err = cos_Theta_error(vtx_pv2B, vtx_pv2B_err, vtx_Bd_p3, vtx_Bd_p3_err)
		BeeKst_CosTheta_err_hist.Fill(BeeKst_CosTheta_err)

c1= ROOT.TCanvas("BeeKst_CosTheta",'Events',800,600)
c1.Divide(2)
c1.cd(1)
BeeKst_CosTheta_hist.Draw('hist')
c1.cd(2)
BeeKst_CosTheta_err_hist.Draw('hist')
c1.SaveAs("CosThetaPlots.png")
"""
