#Prerequist to run the program - Please download ml_config.py and selection_implement_costheta.py and import to your running place first
#coding=utf8
import numpy as np
import ROOT
from array import array
import matplotlib.pyplot as plt
import pandas as pd
import csv
import ml_config
from selection_implement_costheta import cos_Theta_error
from prettytable import PrettyTable
import os, sys

# include the Rootfile = "X" statement if you want it to run over the files in question, else comment it out for those you do not want to be run.

# 1. Choose the channel, rootfile type , cut type and q2 reg range
channel = "muon"
#channel = "electron"/"muon"
Rootfile = "MCJPsi"
#Rootfile = "data"/"MCNR"/"MCJPsi"/"MCPsi2S"
cuts_preselection = "tight"
#cuts = "tight"/"loose"
q2reg = "JPsi"
#q2reg = "NR"/"JPsi"/"Psi2S"/"Sideband"
#Plz select the data_type in section 4 also !!

# 2. Get the ntuples
if channel == "electron":
    if Rootfile == "data":
        periods_s = ['F', 'I', 'O', 'K', 'Q']
        periods_b = ['M', 'L']

        ranges_s = [1, 1, 8, 6, 5]
        ranges_b = [12, 15]

        data_file_s = [["/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08/ntuple-data18_13TeV_period{}_part_0{}.root".format(period, i) for i in range(1, ranges_s[periods_s.index(period)] + 1)] for period in periods_s]
        # print(data_file_s)
        data_file_s[-1].append("/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08_update/ntuple-data18_13TeV_periodQ_part_07.root")
        data_file_b1 = [["/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08/ntuple-data18_13TeV_period{}_part_0{}.root".format(period,i) for i in range(1,10)] for period in periods_b]
        data_file_b1[0].pop(5)
        data_file_b1[1].pop(5)
        data_file_b2 = [["/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08/ntuple-data18_13TeV_period{}_part_{}.root".format(period, i) for i in range(10, ranges_b[periods_b.index(period)] + 1)] for period in periods_b]

        data_file_b = []
        for ind, dfile in enumerate(data_file_b1):
            data_file_b.append(np.concatenate((dfile, data_file_b2[ind])).tolist())
        data_filenms = []
        for data_file in data_file_s:
            data_filenms.append(data_file)
        for data_file in data_file_b:
            data_filenms.append(data_file)
        #print(data_filenms)
        data_filenms = {
          'F': data_filenms[0],
          'I': data_filenms[1],
          'O': data_filenms[2],
          'K': data_filenms[3],
          'Q': data_filenms[4],
          'M': data_filenms[5],
          'L': data_filenms[6]}
        print(data_filenms)
        for i in data_filenms:
            data_files = [ROOT.TFile(name) for name in data_filenms[i]]
        data_tree_files = [dfile.Get("Nominal/BaseSelection_KStaree_BeeKstSelection") for dfile in data_files]
    else:
        data_tree_files = 0

    #Rootfile = "MCNR"  # NEEDS CHANGING FOR V8 FILES - VARIOUS PARTS TO A FILE.
    if Rootfile == "MCNR":
        MC_NR_filenms = ["/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08/ntuple-30059{}_part_01.root".format(i) for i in range(0, 2)]
        MC_NR_files = [ROOT.TFile(name) for name in MC_NR_filenms]
        MC_NR_tree_files = [dfile.Get("Nominal/BaseSelection_KStaree_BeeKstSelection") for dfile in MC_NR_files]
    else:
        MC_NR_tree_files = 0
    #Rootfile = "MCJPsi"
    if Rootfile == "MCJPsi":
        #MC_JPsi_filenms = ["/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08/ntuple-30059{}_part_01.root".format(i) for i in range(2, 3)]
        MC_JPsi_filenms = ["/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08/ntuple-30059{}_part_01.root".format(i) for i in range(2, 4)]
        MC_JPsi_files = [ROOT.TFile(name) for name in MC_JPsi_filenms]
        MC_JPsi_tree_files = [dfile.Get("Nominal/BaseSelection_KStaree_BeeKstSelection") for dfile in MC_JPsi_files]
    else:
        MC_JPsi_tree_files = 0
    #Rootfile = "MCPsi2S"
    if Rootfile == "MCPsi2S":
        MC_Psi2S_filenms = ["/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v05/ntuple-30059{}_part_0_noFlag.root".format(i) for i in range(4, 6)]
        MC_Psi2S_files = [ROOT.TFile(name) for name in MC_Psi2S_filenms]
        MC_Psi2S_tree_files = [dfile.Get("Nominal/BaseSelection_KStaree_BeeKstSelection") for dfile in MC_Psi2S_files]
    else:
        MC_Psi2S_tree_files = 0
elif channel == "muon":
    if Rootfile == "data":
        periods_s = ['K']
        periods_b = ['L']
        data_file_s = ["/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/muon_channel_v0.9/ntuples_updated/ntuple-data18_13TeV_period{}_part_{}{}.root".format(period, i, j) for i in range(0, 1) for j in range(1, 1) for period in periods_s]
        data_file_b = ["/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/muon_channel_v0.9/ntuples_updated/ntuple-data18_13TeV_period{}_part_{}{}{}.root".format(period, k, l, m) for k in range(0, 1) for l in range(0, 1) for m in range(1, 2) for period in periods_b]

        data_filenms = []
        for data_file in data_file_s:
            data_filenms.append(data_file)
        for data_file in data_file_b:
            data_filenms.append(data_file)
        # print(data_filenms)
        #data_filenms = {
        #  'K': data_filenms[0],
        #  'L': data_filenms[1]
        #}
        #print(data_filenms)
        data_filenms = np.array(data_filenms)
        data_files = [ROOT.TFile(name) for name in data_filenms]
        data_tree_files = [dfile.Get("Nominal/BaseSelection_KStarMuMu_BmumuKstSelection") for dfile in data_files]
    else:
        data_tree_files = 0

    # Rootfile = "MCNR"  # NEEDS CHANGING FOR V8 FILES - VARIOUS PARTS TO A FILE.
    if Rootfile == "MCNR":
        MC_NR_filenms = ["/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/muon_channel_v0.9/ntuples_updated/ntuple-30070{}_part_01.root ".format(i) for i in range(0, 2)]
        MC_NR_filenms = np.array(MC_NR_filenms)
        MC_NR_files = [ROOT.TFile(name) for name in MC_NR_filenms]
        MC_NR_tree_files = [dfile.Get("Nominal/BaseSelection_KStarMuMu_BmumuKstSelection") for dfile in MC_NR_files]
    else:
        MC_NR_tree_files = 0
    # Rootfile = "MCJPsi"
    if Rootfile == "MCJPsi":
        # MC_JPsi_filenms = ["/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/v08/ntuple-30059{}_part_01.root".format(i) for i in range(2, 3)]
        MC_JPsi_filenms = ["/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/muon_channel_v0.9/ntuples_updated/ntuple-30070{}_part_01.root".format(i) for i in range(2, 4)]
        MC_JPsi_filenms = np.array(MC_JPsi_filenms)
        MC_JPsi_files = [ROOT.TFile(name) for name in MC_JPsi_filenms]
        MC_JPsi_tree_files = [dfile.Get("Nominal/BaseSelection_KStarMuMu_BmumuKstSelection") for dfile in MC_JPsi_files]
    else:
        MC_JPsi_tree_files = 0
    # Rootfile = "MCPsi2S"
    if Rootfile == "MCPsi2S":
        MC_Psi2S_filenms = ["/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/muon_channel_v0.9/ntuples_updated/ntuple-30070{}_part_01.root".format(i) for i in range(4, 6)]
        MC_Psi2S_filenms = np.array(MC_Psi2S_filenms)
        MC_Psi2S_files = [ROOT.TFile(name) for name in MC_Psi2S_filenms]
        MC_Psi2S_tree_files = [dfile.Get("Nominal/BaseSelection_KStarMuMu_BmumuKstSelection") for dfile in MC_Psi2S_files]
    else:
        MC_Psi2S_tree_files = 0
else:
    print("Channel should be either electron or muon")
    quit()

# 3. Start Preselection
def FlowCutter(tree_files, q2reg, breakdown, truthIDs, select_bestcand= True ,Kst_closer=True ,cuts= cuts_preselection):
    """
    Takes a list of of lists of 'similar' TTree files to perform a standard cutflow on. 'similar' meaning files correspond to Bd/Bdbar for the same overall decay scheme(NR/J/Psi/Psi(2S)) [Res files]/[NR files] for MC [DataPeriod files] for data. Set breakdown to true to print the data print the number of cands and events at each step. Set truthIDs = True if the data is MC with corresponding truth tags. Set q2reg to "NR", "JPsi", or "Psi2S".
    """
    if channel == "electron":
        if cuts == "tight":
            meson_ptcut = 500
            electron_ptcut = 5000
            electron_etacut = 2.5
            meson_etacut = 2.5
            Kst_cut_L = 690
            Kst_cut_H = 1110
            LowBmass_cut = 3000
            HighBmass_cut = 6500

        elif cuts == "loose":
            meson_ptcut = 100
            electron_ptcut = 4000
            Kst_cut_L = 600
            Kst_cut_H = 1200
            LowBmass_cut = 2000
            HighBmass_cut = 7000
        else:
            print("cuts should be either tight or loose")
            quit()
        lim_events = False #Select No of file to run in ntuple
        event_lim = 10000

    elif channel == "muon":
        if cuts == "tight":
            meson_ptcut = 500
            muon_ptcut = 6000
            muon_etacut = 2.5
            meson_etacut = 2.5
            Kst_cut_L = 690
            Kst_cut_H = 1110
            LowBmass_cut = 4700 # Original is 3000, due to cutting event of Trigger "HLT_2mu6_bBmumuxv2_L1LFV-MU6"
            HighBmass_cut = 6000 # Original is 6500

        elif cuts == "loose":
            meson_ptcut = 100
            muon_ptcut = 5000
            Kst_cut_L = 600
            Kst_cut_H = 1200
            LowBmass_cut = 2000
            HighBmass_cut = 7000
        else:
            print("cuts should be either tight or loose")
            quit()
        lim_events = False #select No of file to run in ntuple
        event_lim = 10000

    if tree_files != data_tree_files:
        data_t = "MC"
    else:
        data_t = "data"
    if select_bestcand == False:
        chi2 = "allcands"
    else:
        chi2 = "bestcand"
    if Kst_closer == True:
        Kst_cl = "Kst_closer"
    else:
        Kst_cl = ""
    if data_t == "MC":
        if tree_files == MC_JPsi_tree_files:
            MCfile = "JPsi"
        elif tree_files == MC_NR_tree_files:
            MCfile = "NR"
        elif tree_files == MC_Psi2S_tree_files:
            MCfile = "Psi2S"
        #elif tree_files == MC_Background_tree_files1:
        #    MCfile = "Bkg_Kst0_eta_eeg"
        #elif tree_files == MC_Background_tree_files2:
        #    MCfile = "Bkg_Kst0_eta_mumug"
        #elif tree_files == MC_Background_tree_files3:
        #    MCfile = "Bkg_Kst0_pi0_eeg"
        else:
        #    MCfile = Back_dict[tree_files]
            print("This MC file type not exist yet")
            quit()
        #f = ROOT.TFile("{}/{}{}_{}reg_{}{}_table.root".format(MCfile,data_t, MCfile, q2reg, chi2, Kst_cl),"RECREATE")
        #Create your own file name
        f = ROOT.TFile("TestingMC_cutflow.root", "RECREATE")
        events = ROOT.TTree("tree", "tree")
    else:
        #f = ROOT.TFile("{}/{}_{}reg_{}{}_Period{}_table.root".format(data_t, data_t, q2reg, chi2, Kst_cl, period), "RECREATE")
        #Create your own file name
        f = ROOT.TFile("TestingData_cutflow.root", "RECREATE")
        events = ROOT.TTree("tree", "tree")
    if channel == "electron":
        B_m = array('f', [0.])
        Dielec_m = array('f', [0.])
        e0_pt_m = array('f', [0.])
        e1_pt_m = array('f', [0.])
        e0_eta_m = array('f', [0.])
        e1_eta_m = array('f', [0.])
        mes0_pt_m = array('f', [0.])
        mes1_pt_m = array('f', [0.])
        mes0_eta_m = array('f', [0.])
        mes1_eta_m = array('f', [0.])
        tau_m = array('f', [0.])
        tau_over_err = array('f', [0.])
        Lxy_m = array('f', [0.])
        Lxy_over_err = array('f', [0.])
        ID_m = array('f', [0.])
        e0_phi_m = array('f', [0.])
        e1_phi_m = array('f', [0.])
        B_chi2_m = array('f', [0.])
        dielectron_px_m = array('f', [0.])
        dimeson_px_m = array('f', [0.])
        e0_iso_c40_m = array('f', [0.])
        e1_iso_c40_m = array('f', [0.])
        B_pT_m = array('f', [0.])  # additional inputs

        cos_theta = array('f', [0.])
        cos_theta_err = array('f', [0.])

        # A branch for the B-meson mass for resonant (J/Psi) decays for decays of
        # both Bd and BdBar mesons

        events.Branch("BeeKst_B_mass", B_m, 'BeeKst_B_mass/F')
        events.Branch("BeeKst_diElectron_mass",Dielec_m,'BeeKst_diElectron_mass/F')
        events.Branch("BeeKst_electron0_pT", e0_pt_m, 'BeeKst_electron0_pT/F')
        events.Branch("BeeKst_electron1_pT", e1_pt_m, 'BeeKst_electron1_pT/F')
        events.Branch("BeeKst_electron0_eta", e0_eta_m, 'BeeKst_electron0_eta/F')
        events.Branch("BeeKst_electron1_eta", e1_eta_m, 'BeeKst_electron1_eta/F')
        events.Branch("BeeKst_meson0_pT", mes0_pt_m, 'BeeKst_meson0_pT/F')
        events.Branch("BeeKst_meson1_pT", mes1_pt_m, 'BeeKst_meson1_pT/F')
        events.Branch("BeeKst_meson0_eta", mes0_eta_m, 'BeeKst_meson0_eta/F')
        events.Branch("BeeKst_meson1_eta", mes1_eta_m, 'BeeKst_meson1_eta/F')
        events.Branch("BeeKst_B_tau_constM_PVMinA0", tau_m, 'BeeKst_B_tau_constM_PVMinA0/F')
        events.Branch("BeeKst_B_tau_over_err",tau_over_err,'BeeKst_B_tau_over_err/F')
        events.Branch("BeeKst_Lxy_minA0", Lxy_m, 'BeeKst_Lxy_minA0/F')
        events.Branch("BeeKst_Lxy_over_err", Lxy_over_err, 'BeeKst_Lxy_over_err/F')
        events.Branch("BeeKst_electron0_phi", e0_phi_m, 'BeeKst_electron0_phi/F')
        events.Branch("BeeKst_electron1_phi", e1_phi_m, 'BeeKst_electron1_phi/F')
        events.Branch("BeeKst_chi2", B_chi2_m, 'BeeKst_chi2/F')
        events.Branch("BeeKst_diElectron_px",dielectron_px_m,'BeeKst_diElectron_px/F')
        events.Branch("BeeKst_diMeson_px", dimeson_px_m, 'BeeKst_diMeson_px/F')
        events.Branch("BeeKst_electron0_iso_c40",e0_iso_c40_m,'BeeKst_electron0_iso_c40/F')
        events.Branch("BeeKst_electron1_iso_c40",e1_iso_c40_m,'BeeKst_electron1_iso_c40/F')
        events.Branch("BeeKst_diElectron_pT", B_pT_m, 'BeeKst_diElectron_pT/F')
        events.Branch("BeeKst_CosTheta",cos_theta,'cos_theta/F')
        events.Branch("BeeKst_CosTheta_err",cos_theta_err,'cos_theta_err/F')

        if truthIDs == True:
            events.Branch("BeeKst_isTrue", ID_m, 'BeeKst_isTrue/F')
        attr_names = [
            "BeeKst_electron0_pT",
            "BeeKst_electron1_pT",
            "BeeKst_electron0_eta",
            "BeeKst_electron1_eta",
            "BeeKst_meson0_pT",
            "BeeKst_meson1_pT",
            "BeeKst_meson0_eta",
            "BeeKst_meson1_eta",
            "BeeKst_chi2_over_nDoF",
            "BeeKst_kaonPion_mass",
            "BeeKst_pionKaon_mass",
            "BeeKst_B_tau_constM_PVMinA0",
            "BeeKst_B_tau_constM_PVMinA0_err",
            "BeeKst_Bbar_tau_constM_PVMinA0",
            "BeeKst_Bbar_tau_constM_PVMinA0_err",
            "BeeKst_B_mass",
            "BeeKst_Bbar_mass",
            "BeeKst_Lxy_minA0",
            "BeeKst_Lxy_minA0_err",
            "BeeKst_diElectron_gsf_id_isOK",
            "BeeKst_electron0_charge",
            "BeeKst_electron1_charge",
            "BeeKst_meson0_charge",
            "BeeKst_meson1_charge",
            "BeeKst_electron0_passLoose",
            "BeeKst_electron1_passLoose",
            "BeeKst_meson0_passLoose",
            "BeeKst_meson1_passLoose",
            "BeeKst_electron0_passLooseElectron",
            "BeeKst_electron1_passLooseElectron",
            "BeeKst_electron0_phi",
            "BeeKst_electron1_phi",
            "BeeKst_chi2",
            "BeeKst_diElectron_px",
            "BeeKst_diMeson_px",
            "BeeKst_electron0_iso_c40",
            "BeeKst_electron1_iso_c40",
            "BeeKst_diElectron_pT"]

    elif channel == "muon":
        B_m = array('f', [0.])
        Bbar_m = array('f', [0.])
        Dimuon_m = array('f', [0.])
        mu0_pt_m = array('f', [0.])
        mu1_pt_m = array('f', [0.])
        mu0_eta_m = array('f', [0.])
        mu1_eta_m = array('f', [0.])
        mes0_pt_m = array('f', [0.])
        mes1_pt_m = array('f', [0.])
        mes0_eta_m = array('f', [0.])
        mes1_eta_m = array('f', [0.])
        tau_m = array('f', [0.])
        tau_over_err = array('f', [0.])
        Lxy_m = array('f', [0.])
        Lxy_over_err = array('f', [0.])
        ID_m = array('f', [0.])
        mu0_phi_m = array('f', [0.])
        mu1_phi_m = array('f', [0.])
        B_chi2_m = array('f', [0.])
        dimuon_px_m = array('f', [0.])
        dimeson_px_m = array('f', [0.])
        mu0_iso_c40_m = array('f', [0.])
        mu1_iso_c40_m = array('f', [0.])
        acc_trigger = array('f', [0.])
        B_pT_m = array('f', [0.])  # additional inputs

        cos_theta = array('f', [0.])
        cos_theta_err = array('f', [0.])

        # A branch for the B-meson mass for resonant (J/Psi) decays for decays of
        # both Bd and BdBar mesons
        events.Branch("BmumuKst_B_mass", B_m, 'BmumuKst_B_mass/F')
        events.Branch("BmumuKst_Bbar_mass", Bbar_m, 'BmumuKst_Bbar_mass/F')
        events.Branch("BmumuKst_diMuon_mass", Dimuon_m, 'BmumuKst_diMuon_mass/F')
        events.Branch("BmumuKst_muon0_pT", mu0_pt_m, 'BmumuKst_muon0_pT/F')
        events.Branch("BmumuKst_muon1_pT", mu1_pt_m, 'BmumuKst_muon1_pT/F')
        events.Branch("BmumuKst_muon0_eta", mu0_eta_m, 'BmumuKst_muon0_eta/F')
        events.Branch("BmumuKst_muon1_eta", mu1_eta_m, 'BmumuKst_muon1_eta/F')
        events.Branch("BmumuKst_meson0_pT", mes0_pt_m, 'BmumuKst_meson0_pT/F')
        events.Branch("BmumuKst_meson1_pT", mes1_pt_m, 'BmumuKst_meson1_pT/F')
        events.Branch("BmumuKst_meson0_eta", mes0_eta_m, 'BmumuKst_meson0_eta/F')
        events.Branch("BmumuKst_meson1_eta", mes1_eta_m, 'BmumuKst_meson1_eta/F')
        events.Branch("BmumuKst_B_tau_constM_PVMinA0", tau_m, 'BmumuKst_B_tau_constM_PVMinA0/F')
        events.Branch("BmumuKst_B_tau_over_err", tau_over_err, 'BmumuKst_B_tau_over_err/F')
        events.Branch("BmumuKst_BinA0", Lxy_m, 'BmumuKst_Lxy_minA0/F')
        events.Branch("BmumuKst_Lxy_over_err", Lxy_over_err, 'BmumuKst_Lxy_over_err/F')
        events.Branch("BmumuKst_muon0_phi", mu0_phi_m, 'BmumuKst_muon0_phi/F')
        events.Branch("BmumuKst_muon1_phi", mu1_phi_m, 'BmumuKst_muon1_phi/F')
        events.Branch("BmumuKst_chi2", B_chi2_m, 'BmumuKst_chi2/F')
        events.Branch("BmumuKst_diMuon_px", dimuon_px_m, 'BmumuKst_diMuon_px/F')
        events.Branch("BmumuKst_diMeson_px", dimeson_px_m, 'BmumuKst_diMeson_px/F')
        events.Branch("BmumuKst_muon0_iso_c40", mu0_iso_c40_m, 'BmumuKst_muon0_iso_c40/F')
        events.Branch("BmumuKst_muon1_iso_c40", mu1_iso_c40_m, 'BmumuKst_muon1_iso_c40/F')
        events.Branch("BmumuKst_diMuon_pT", B_pT_m, 'BmumuKst_diMuon_pT/F')
        events.Branch("BmumuKst_CosTheta", cos_theta, 'cos_theta/F')
        events.Branch("BmumuKst_CosTheta_err", cos_theta_err, 'cos_theta_err/F')
        events.Branch("accepted_triggers", acc_trigger, 'accepted_triggers/F')

        if truthIDs == True:
            events.Branch("BmumuKst_isTrue", ID_m, 'BmumuKst_isTrue/F')

        attr_names = [
            "BmumuKst_muon0_pT",
            "BmumuKst_muon1_pT",
            "BmumuKst_muon0_eta",
            "BmumuKst_muon1_eta",
            "BmumuKst_meson0_pT",
            "BmumuKst_meson1_pT",
            "BmumuKst_meson0_eta",
            "BmumuKst_meson1_eta",
            "BmumuKst_chi2_over_nDoF",
            "BmumuKst_kaonPion_mass",
            "BmumuKst_pionKaon_mass",
            "BmumuKst_B_tau_constM_PVMinA0",
            "BmumuKst_B_tau_constM_PVMinA0_err",
            "BmumuKst_Bbar_tau_constM_PVMinA0",
            "BmumuKst_Bbar_tau_constM_PVMinA0_err",
            "BmumuKst_B_mass",
            "BmumuKst_Bbar_mass",
            "BmumuKst_Lxy_minA0",
            "BmumuKst_Lxy_minA0_err",
            "BmumuKst_diMuon_gsf_id_isOK",
            "BmumuKst_muon0_charge",
            "BmumuKst_muon1_charge",
            "BmumuKst_meson0_charge",
            "BmumuKst_meson1_charge",
            "BmumuKst_muon0_passLoose",
            "BmumuKst_muon1_passLoose",
            "BmumuKst_meson0_passLoose",
            "BmumuKst_meson1_passLoose",
            "BmumuKst_muon0_passLooseElectron",
            "BmumuKst_muon1_passLooseElectron",
            "BmumuKst_muon0_phi",
            "BmumuKst_muon1_phi",
            "BmumuKst_chi2",
            "BmumuKst_diMuon_px",
            "BmumuKst_diMeson_px",
            "BmumuKst_muon0_iso_c40",
            "BmumuKst_muon1_iso_c40",
            "BmumuKst_diMuon_pT",
            "accepted_triggers"]

    # ------------Global Constants---------
    # actually this 900 to match with Tomas' cutflow 895.81
    m_Kst = 895.81
    m_Bd = 5279.61
    if channel == "electron":
        if select_bestcand == False:
            cuttingvarno = 13
        else:
            cuttingvarno = 14
    else:
        if select_bestcand == False:
            cuttingvarno = 15
        else:
            cuttingvarno = 16
    
    ROOT.TH1.SetDefaultSumw2()
    No_cands_pre = ROOT.TH1D("h1_f1", "No_cands_pre", 15, -0.5, 14.5)
    No_cands_post = ROOT.TH1D("h2_f1", "No_cands_post", 5, -0.5, 4.5)
    h0 = ROOT.TH1D("h0", "B mass using K_closer after preselection", 100, 4700, 5700)
    h1 = ROOT.TH1D("h1", "B and B bar mass after preselection", 100, 4700, 5700)
    #h2 = ROOT.TH1D("h2", "kaonPion mass after preselection", 100, 750, 1100)
    #h3 = ROOT.TH1D("h3", "Dimuon mass after only trigger selection", 100, 2000, 4000)
    #h4 = ROOT.TH1D("h4", "Dimuon mass after preselection", 100, 2000, 4000)
    counters = [0 for i in range(cuttingvarno)]
    event_tokens = [0 for i in range(cuttingvarno)]

    if truthIDs == True:
        Truth_event_tokens = [0 for i in range(cuttingvarno)]
        Truth_counters = [0 for i in range(cuttingvarno)]
        No_truth_pre = ROOT.TH1D("h3_f1", "No_truths_pre", 5, -0.5, 4.5)
        No_truth_post = ROOT.TH1D("h4_f1", "No_truths_post", 5, -0.5, 4.5)

    for tree_no, tree in enumerate(tree_files):
        double_matched = 0
        print("Period:", tree_no)
        n_events = tree.GetEntries()
        print(n_events)

        if lim_events == True:
            n_events = event_lim
            if tree.GetEntries() < n_events:
                n_events = tree.GetEntries()
            print(n_events)  # _finish

        event_counter = 0
        for i in range(n_events):  # n_events_start,n_events_finish):
            tree.GetEntry(i)
            if event_counter % 10000 == 0:
                print(event_counter)

            if channel == "electron":
                Dielec_mass = getattr(tree, "BeeKst_diElectron_mass")
                e0_pt = getattr(tree, attr_names[0])
                e1_pt = getattr(tree, attr_names[1])
                e0_eta = getattr(tree, attr_names[2])
                e1_eta = getattr(tree, attr_names[3])
                mes0_pt = getattr(tree, attr_names[4])
                mes1_pt = getattr(tree, attr_names[5])
                mes0_eta = getattr(tree, attr_names[6])
                mes1_eta = getattr(tree, attr_names[7])
                chi2_over_nDoF = getattr(tree, attr_names[8])
                m_K_Pi = getattr(tree, attr_names[9])
                m_Pi_K = getattr(tree, attr_names[10])
                tau_B = getattr(tree, attr_names[11])
                tau_B_err = getattr(tree, attr_names[12])
                tau_Bbar = getattr(tree, attr_names[13])
                tau_Bbar_err = getattr(tree, attr_names[14])
                m_B = getattr(tree, attr_names[15])
                m_BBar = getattr(tree, attr_names[16])
                Lxy = getattr(tree, attr_names[17])
                Lxy_err = getattr(tree, attr_names[18])
                BeeKst_diElectron_ID_OK = getattr(tree, attr_names[19])
                e0_charge = getattr(tree, attr_names[20])
                e1_charge = getattr(tree, attr_names[21])
                m0_charge = getattr(tree, attr_names[22])
                m1_charge = getattr(tree, attr_names[23])
                e0_PL = getattr(tree, attr_names[24])
                e1_PL = getattr(tree, attr_names[25])
                m0_PL = getattr(tree, attr_names[26])
                m1_PL = getattr(tree, attr_names[27])
                e0_PLe = getattr(tree, attr_names[28])
                e1_PLe = getattr(tree, attr_names[29])
                e0_phi = getattr(tree, "BeeKst_electron0_phi")
                e1_phi = getattr(tree, "BeeKst_electron1_phi")
                B_chi2 = getattr(tree, attr_names[32])
                dielectron_px = getattr(tree, attr_names[33])
                dimeson_px = getattr(tree, attr_names[34])
                e0_iso_c40 = getattr(tree, attr_names[35])
                e1_iso_c40 = getattr(tree, attr_names[36])
                B_pT = getattr(tree, attr_names[37])

                vtx_BeeKst_PV_minA0_x_err = getattr(tree, "BeeKst_PV_minA0_x_err")
                vtx_BeeKst_PV_minA0_y_err = getattr(tree, "BeeKst_PV_minA0_y_err")
                vtx_BeeKst_PV_minA0_z_err = getattr(tree, "BeeKst_PV_minA0_z_err")
                vtx_BeeKst_x_err = getattr(tree, "BeeKst_x_err")
                vtx_BeeKst_y_err = getattr(tree, "BeeKst_y_err")
                vtx_BeeKst_z_err = getattr(tree, "BeeKst_z_err")
                vtx_BeeKst_x = getattr(tree, "BeeKst_x")
                vtx_BeeKst_y = getattr(tree, "BeeKst_y")
                vtx_BeeKst_z = getattr(tree, "BeeKst_z")
                vtx_BeeKst_PV_minA0_x = getattr(tree, "BeeKst_PV_minA0_x")
                vtx_BeeKst_PV_minA0_y = getattr(tree, "BeeKst_PV_minA0_y")
                vtx_BeeKst_PV_minA0_z = getattr(tree, "BeeKst_PV_minA0_z")
                mes0_phi = getattr(tree, "BeeKst_meson0_phi")
                mes1_phi = getattr(tree, "BeeKst_meson1_phi")

                if truthIDs == True and data_t == "MC":
                    if MCfile == "NR":
                        TruthIDs_Bd = getattr(tree, "BeeKst_isTrueNonresBd")  # [0,0,1,1,0,0],
                        TruthIDs_Bdbar = getattr(tree, "BeeKst_isTrueNonresBdbar")  # [0,0,0,0,0]
                        # print(TruthIDs_Bd)
                    elif MCfile == "JPsi":
                        TruthIDs_Bd = getattr(tree, "BeeKst_isTrueResBd")  # [0,0,1,1,0,0],
                        TruthIDs_Bdbar = getattr(tree, "BeeKst_isTrueResBdbar")  # [0,0,0,0,0]
                        # print(TruthIDs_Bdbar)
                if truthIDs == True:
                    Truth_event_counters = [0 for j in range(cuttingvarno)]
                    Truth_OG_cands = 0  # to be incremented in loop
                    Truth_post_candids = 0
                event_counters = [0 for j in range(cuttingvarno)]
                OG_cands = len(e0_pt)
                post_candids = 0  # No of candidates past cutflow in each event
                pass_cands = []  # list of the chi^2 of the post_cands and what they're indentified as, used to select them assigned IDs of the cands that pass the cutflow (0 - Bd, 1 -Bdbar)
                post_ids = []
                post_inds = []  # indices of the cands that pass the cutflow
                DMs= []

                post_Costhetas = [0 for cand in m_B]
                post_Costheta_errs = [0 for cand in m_B]

                for ind, var in enumerate(m_B):
                    B_matched = False
                    Bbar_matched = False
                    B_IDs = []
                    B_passed, K_passed, qual_passed = False, False, False
                    q2crit = False  # Set to True to remove q2 cuts

                    q2 = (Dielec_mass[ind] / 1000)**2

                    if q2reg == "NR" and 1.1 < q2 < 6:
                        q2crit = True
                    elif q2reg == "JPsi" and 6 < q2 < 11:
                        q2crit = True
                    elif q2reg == "Psi2S" and 11 < q2 < 16:
                        q2crit = True
                    elif q2reg == "Sideband" and 1.1 < q2 < 6 and (m_B[ind] < 4000. or m_BBar[ind]<4000. or m_B[ind] >5700. or m_BBar[ind]>5700.):
                        q2crit = True
                    if data_t == "data" and q2reg == "NR":
                        print("Probably shouldn't be doing that :p")
                        return
                    if q2crit == True:
                        counters[0] += 1
                        event_counters[0] = 1
                        if truthIDs == True:
                            if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                Truth_counters[0] += 1
                                Truth_OG_cands += 1
                                Truth_event_counters[0] = 1
                        if e0_charge[ind]*e1_charge[ind] < 0 and m0_charge[ind]*m1_charge[ind] < 0:
                            counters[1] += 1
                            event_counters[1] = 1
                            if truthIDs == True:
                                if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                    Truth_counters[1] += 1
                                    Truth_event_counters[1] = 1
                            if BeeKst_diElectron_ID_OK[ind] == 1:
                                counters[2] += 1
                                event_counters[2] = 1
                                if truthIDs == True:
                                    if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                        Truth_counters[2] += 1
                                        Truth_event_counters[2] = 1

                                vtx_e0p4 = ROOT.TLorentzVector()
                                vtx_e0p4.SetPtEtaPhiM(e0_pt[ind], e0_eta[ind], e0_phi[ind], ml_config.PDG["e_mass"])
                                vtx_e1p4 = ROOT.TLorentzVector()
                                vtx_e1p4.SetPtEtaPhiM(e1_pt[ind], e1_eta[ind], e1_phi[ind], ml_config.PDG["e_mass"])
                                vtx_dRee = vtx_e0p4.DeltaR(vtx_e1p4)

                                if vtx_dRee > 0.1:
                                    counters[3] += 1
                                    event_counters[3] = 1
                                    if truthIDs == True:
                                        if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                            Truth_counters[3] += 1
                                            Truth_event_counters[3] = 1

                                    if e0_pt[ind] >= electron_ptcut and e1_pt[ind] >= electron_ptcut:
                                        counters[4] += 1
                                        event_counters[4] = 1
                                        if truthIDs == True:
                                            if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                Truth_counters[4] += 1
                                                Truth_event_counters[4] = 1

                                        if abs(e0_eta[ind]) <= electron_etacut and abs(e1_eta[ind]) <= electron_etacut:
                                                counters[5] += 1
                                                event_counters[5] = 1
                                                if truthIDs == True:
                                                    if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                        Truth_counters[5] += 1
                                                        Truth_event_counters[5] = 1

                                                if mes0_pt[ind] >= meson_ptcut and mes1_pt[ind] >= meson_ptcut:
                                                    counters[6] += 1
                                                    event_counters[6] = 1
                                                    if truthIDs == True:
                                                        if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                            Truth_counters[6] += 1
                                                            Truth_event_counters[6] = 1

                                                    if abs(mes0_eta[ind]) < meson_etacut and abs(mes1_eta[ind]) < meson_etacut:
                                                        counters[7] += 1
                                                        event_counters[7] = 1
                                                        if truthIDs == True:
                                                            if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                                Truth_counters[7] += 1
                                                                Truth_event_counters[7] = 1

                                                        if q2 <= 49 :
                                                            counters[8] += 1
                                                            event_counters[8] = 1
                                                            if truthIDs == True:
                                                                if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                                    Truth_counters[8] += 1
                                                                    Truth_event_counters[8] = 1

                                                            if LowBmass_cut <= m_B[ind] <= HighBmass_cut:
                                                                counters[9] += 1
                                                                event_counters[9] = 1
                                                                B_passed = True
                                                                if truthIDs:
                                                                    if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                                        Truth_counters[9] += 1
                                                                        Truth_event_counters[9] = 1
                                                                        Truth_post_candids += 1

                                                                if Kst_cut_L <= m_K_Pi[ind] <= Kst_cut_H:
                                                                    K_passed = True
                                                                    counters[10] += 1
                                                                    event_counters[10] = 1
                                                                    if truthIDs:
                                                                        if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                                            Truth_counters[10] += 1
                                                                            Truth_event_counters[10] = 1

                                                                    if [e0_PL[ind], e1_PL[ind], m0_PL[ind],m1_PL[ind], e0_PLe[ind], e1_PLe[ind]] == [1,1,1,1,1,1]:
                                                                        qual_passed = True
                                                                        counters[11] += 1
                                                                        event_counters[11] = 1
                                                                        B_matched = True
                                                                        B_IDs.append(0)
                                                                        if truthIDs:
                                                                            if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                                                Truth_counters[11] += 1
                                                                                Truth_event_counters[11] = 1
                                                                        # append with chi2
                                                                        pass_cands.append(chi2_over_nDoF[ind])
                                                                        post_candids += 1
                                                                        # B (0) or Bbar (1), according to the alg.
                                                                        post_ids.append(0)
                                                                        post_inds.append(ind)
                                                            if LowBmass_cut <= m_BBar[ind] <= HighBmass_cut:
                                                                counters[9] += 1
                                                                event_counters[9] = 1
                                                                if truthIDs:
                                                                    if TruthIDs_Bdbar[ind] == 1 or TruthIDs_Bd[ind] == 1:
                                                                        Truth_counters[9] += 1
                                                                        Truth_event_counters[9] = 1
                                                                        Truth_post_candids += 1

                                                                if Kst_cut_L <= m_Pi_K[ind] <= Kst_cut_H:
                                                                    counters[10] += 1
                                                                    event_counters[10] = 1
                                                                    if truthIDs:
                                                                        if TruthIDs_Bdbar[ind] == 1 or TruthIDs_Bd[ind] == 1:
                                                                            Truth_counters[10] += 1
                                                                            Truth_event_counters[10] = 1

                                                                    if [e0_PL[ind], e1_PL[ind], m0_PL[ind],m1_PL[ind], e0_PLe[ind], e1_PLe[ind]] == [1, 1, 1, 1, 1, 1]:
                                                                        Bbar_matched = True
                                                                        B_IDs.append(1)
                                                                        counters[11] += 1
                                                                        event_counters[11] = 1
                                                                        if truthIDs == True:
                                                                            if TruthIDs_Bdbar[ind] == 1 or TruthIDs_Bd[ind] == 1:
                                                                                Truth_counters[11] += 1
                                                                                Truth_event_counters[11] = 1
                                                                        # append with chi2
                                                                        pass_cands.append(chi2_over_nDoF[ind])
                                                                        post_candids += 1
                                                                        # B (0) or Bbar (1), according to the alg.
                                                                        post_ids.append(1)
                                                                        post_inds.append(ind)

                                                            if B_matched and Bbar_matched:
                                                                DMs.append(True)
                                                                DMs.append(True)
                                                                double_matched += 1
                                                            h1.Fill(m_B[ind])
                                                            print(m_B[ind])
                                                            #Originally is nested if loop; test the Kst_closer function so change to "no nested if loop" manually
                                                            if Kst_closer == True:
                                                                # chooses between a single candidate being counted as Bd or Bdbar.
                                                                if abs(m_Pi_K[ind] - m_Kst) < abs(m_K_Pi[ind] - m_Kst):
                                                                    del B_IDs[0]
                                                                    h0.Fill(m_B[ind])
                                                                    print(m_B[ind])
                                                                else:
                                                                    del B_IDs[1]

                                                            if B_matched or Bbar_matched:
                                                                if B_matched * Bbar_matched == 0:
                                                                    DMs.append(False)
                                                                counters[12] += 1
                                                                event_counters[12] = 1
                                                                if truthIDs:
                                                                    if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                                        Truth_counters[12] += 1
                                                                        Truth_event_counters[12] = 1

                                                                tau_B_over_err = tau_B[ind] / tau_B_err[ind]
                                                                Lxy_B_over_err = Lxy[ind] / Lxy_err[ind]

                                                                # -----------------------------Defining CosTheta------------------------------
                                                                vtx_Bd_vtx = ROOT.TVector3()
                                                                vtx_Bd_vtx.SetXYZ(vtx_BeeKst_x[ind],vtx_BeeKst_y[ind],vtx_BeeKst_z[ind])
                                                                vtx_Bd_pv = ROOT.TVector3()
                                                                vtx_Bd_pv.SetXYZ(vtx_BeeKst_PV_minA0_x[ind],vtx_BeeKst_PV_minA0_y[ind],vtx_BeeKst_PV_minA0_z[ind])
                                                                vtx_pv2B = vtx_Bd_vtx - vtx_Bd_pv

                                                                vtx_e0p4 = ROOT.TLorentzVector()
                                                                vtx_e0p4.SetPtEtaPhiM(e0_pt[ind], e0_eta[ind],e0_phi[ind], ml_config.PDG["e_mass"])
                                                                vtx_e1p4 = ROOT.TLorentzVector()
                                                                vtx_e1p4.SetPtEtaPhiM(e1_pt[ind], e1_eta[ind],e1_phi[ind],ml_config.PDG["e_mass"])
                                                                vtx_diElectron_p4 = vtx_e0p4 + vtx_e1p4

                                                                vtx_m0_pi_p4 = ROOT.TLorentzVector()
                                                                vtx_m0_pi_p4.SetPtEtaPhiM(mes0_pt[ind],mes0_eta[ind],mes0_phi[ind], ml_config.PDG["pipm_mass"])
                                                                vtx_m0_K_p4 = ROOT.TLorentzVector()
                                                                vtx_m0_K_p4.SetPtEtaPhiM(mes0_pt[ind],mes0_eta[ind],mes0_phi[ind],ml_config.PDG["Kpm_mass"])
                                                                vtx_m1_pi_p4 = ROOT.TLorentzVector()
                                                                vtx_m1_pi_p4.SetPtEtaPhiM(mes1_pt[ind],mes1_eta[ind],mes1_phi[ind],ml_config.PDG["pipm_mass"])
                                                                vtx_m1_K_p4 = ROOT.TLorentzVector()
                                                                vtx_m1_K_p4.SetPtEtaPhiM(mes1_pt[ind], mes1_eta[ind], mes1_phi[ind], ml_config.PDG["Kpm_mass"])

                                                                vtx_Kpi_p4 = vtx_m0_K_p4 + vtx_m1_pi_p4
                                                                vtx_piK_p4 = vtx_m0_pi_p4 + vtx_m1_K_p4
                                                                vtx_Bd_p4 = vtx_diElectron_p4 + vtx_Kpi_p4
                                                                vtx_Bdbar_p4 = vtx_diElectron_p4 + vtx_piK_p4

                                                                vtx_Bd_p3 = vtx_Bd_p4.Vect()
                                                                vtx_Bd_p3_err = ROOT.TVector3()
                                                                vtx_Bd_p3_err.SetXYZ(0., 0., 0.)
                                                                vtx_Bd_pv_err = ROOT.TVector3()
                                                                vtx_Bd_pv_err.SetXYZ(vtx_BeeKst_PV_minA0_x_err[ind],vtx_BeeKst_PV_minA0_y_err[ind],vtx_BeeKst_PV_minA0_z_err[ind])

                                                                vtx_Bd_vtx_err = ROOT.TVector3()
                                                                vtx_Bd_vtx_err.SetXYZ(vtx_BeeKst_x_err[ind], vtx_BeeKst_y_err[ind], vtx_BeeKst_z_err[ind])
                                                                vtx_pv2B_err = ROOT.TVector3()
                                                                vtx_pv2B_err.SetXYZ(np.sqrt(vtx_Bd_vtx_err.X() * vtx_Bd_vtx_err.X() + vtx_Bd_pv_err.X() * vtx_Bd_pv_err.X()),
                                                                                        np.sqrt(vtx_Bd_vtx_err.Y() * vtx_Bd_vtx_err.Y() + vtx_Bd_pv_err.Y() * vtx_Bd_pv_err.Y()),
                                                                                        np.sqrt(vtx_Bd_vtx_err.Z() * vtx_Bd_vtx_err.Z() + vtx_Bd_pv_err.Z() * vtx_Bd_pv_err.Z()))
                                                                BeeKst_CosTheta = vtx_pv2B.Dot(vtx_Bd_p3) / np.sqrt(vtx_pv2B.Dot(vtx_pv2B) * vtx_Bd_p3.Dot(vtx_Bd_p3))
                                                                BeeKst_CosTheta_err = cos_Theta_error(vtx_pv2B, vtx_pv2B_err,vtx_Bd_p3,vtx_Bd_p3_err)
                                                                post_Costhetas[ind] = BeeKst_CosTheta
                                                                post_Costheta_errs[ind] = BeeKst_CosTheta / BeeKst_CosTheta_err
                                                                # ----------------------------------------
                                                                if select_bestcand:
                                                                    Dielec_m[0] = Dielec_mass[ind]
                                                                    e0_pt_m[0] = e0_pt[ind]
                                                                    e1_pt_m[0] = e1_pt[ind]
                                                                    e0_eta_m[0] = e0_eta[ind]
                                                                    e1_eta_m[0] = e1_eta[ind]
                                                                    mes0_pt_m[0] = mes0_pt[ind]
                                                                    mes1_pt_m[0] = mes1_pt[ind]
                                                                    mes0_eta_m[0] = mes0_eta[ind]
                                                                    mes1_eta_m[0] = mes1_eta[ind]
                                                                    tau_m[0] = tau_B[ind]
                                                                    tau_over_err[0] = tau_B_over_err
                                                                    Lxy_m[0] = Lxy[ind]
                                                                    Lxy_over_err[0] = Lxy_B_over_err
                                                                    e0_phi_m[0] = e0_phi[ind]
                                                                    e1_phi_m[0] = e1_phi[ind]
                                                                    B_chi2_m[0] = B_chi2[ind]
                                                                    dielectron_px_m[0] = dielectron_px[ind]
                                                                    dimeson_px_m[0] = dimeson_px[ind]
                                                                    e0_iso_c40_m[0] = e0_iso_c40[ind]
                                                                    e1_iso_c40_m[0] = e1_iso_c40[ind]
                                                                    B_pT_m[0] = B_pT[ind]
                                                                    cos_theta[0] = BeeKst_CosTheta
                                                                    cos_theta_err[0] = BeeKst_CosTheta / BeeKst_CosTheta_err
                                                                    for B_id in B_IDs:
                                                                        if B_id == 0:
                                                                            B_m[0] = m_B[ind]
                                                                        if B_id == 1:
                                                                            B_m[0] = m_BBar[ind]

                                                                        if truthIDs:
                                                                            if B_id == 0:
                                                                                ID_m[0] = TruthIDs_Bd[ind]
                                                                            if B_id == 1:
                                                                                ID_m[0] = TruthIDs_Bdbar[ind]
                                                                        if q2reg == "Sideband":
                                                                            if B_m[0] < 4000. or B_m[0] > 5700.:
                                                                                events.Fill()
                                                                        else:
                                                                             events.Fill()

                # ----------Event basis------------- #Chooses between multiple Bd or Bdbar candidates in an event
                if select_bestcand == True:
                    if pass_cands != []:
                        # print("Passed candidate chi2/ndofs", pass_cands)
                        # index of passed cand with lowest chi2 [pass1_B , pass1_Bbar, .... ]
                        selection_index = np.argmin(pass_cands)
                        # print("highest cand ind:", selection_index)
                        # the index in the event list of candidates that corresponds to lowest chi2 [cand1, cand2,...]
                        cand_ind = post_inds[selection_index]
                        # print("post indexes", post_inds)
                        # print("cand ind:", cand_ind)
                        # print("DMs:", DMs)
                        counters[13] += 1
                        event_counters[13] = 1
                        q2 = (Dielec_mass[cand_ind] / 1000) ** 2
                        if truthIDs == True:
                            if TruthIDs_Bdbar[cand_ind] == 1 or TruthIDs_Bd[cand_ind] == 1:
                                Truth_counters[13] += 1
                                Truth_event_counters[13] = 1
                        e0_pt_m[0] = e0_pt[cand_ind]
                        e1_pt_m[0] = e1_pt[cand_ind]
                        e0_eta_m[0] = e0_eta[cand_ind]
                        e1_eta_m[0] = e1_eta[cand_ind]
                        mes0_pt_m[0] = mes0_pt[cand_ind]
                        mes1_pt_m[0] = mes1_pt[cand_ind]
                        mes0_eta_m[0] = mes0_eta[cand_ind]
                        mes1_eta_m[0] = mes1_eta[cand_ind]
                        Dielec_m[0] = Dielec_mass[cand_ind]

                        tau_B_over_err = tau_B[cand_ind] / tau_B_err[cand_ind]
                        Lxy_B_over_err = Lxy[cand_ind] / Lxy_err[cand_ind]
                        tau_m[0] = tau_B[cand_ind]
                        tau_over_err[0] = tau_B_over_err
                        Lxy_m[0] = Lxy[cand_ind]
                        Lxy_over_err[0] = Lxy_B_over_err
                        cos_theta[0] = post_Costhetas[cand_ind]
                        cos_theta_err[0] = post_Costheta_errs[cand_ind]

                        e0_phi_m[0] = e0_phi[cand_ind]
                        e1_phi_m[0] = e1_phi[cand_ind]
                        B_chi2_m[0] = B_chi2[cand_ind]
                        dielectron_px_m[0] = dielectron_px[cand_ind]
                        dimeson_px_m[0] = dimeson_px[cand_ind]
                        e0_iso_c40_m[0] = e0_iso_c40[cand_ind]
                        e1_iso_c40_m[0] = e1_iso_c40[cand_ind]
                        B_pT_m[0] = B_pT[cand_ind]

                        if Kst_closer == True or DMs[selection_index] == False:
                            if post_ids[selection_index] == 0:
                                B_m[0] = m_B[cand_ind]
                            if post_ids[selection_index] == 1:
                                B_m[0] = m_BBar[cand_ind]
                            if truthIDs == True:
                                if post_ids[selection_index] == 0:
                                    ID_m[0] = TruthIDs_Bd[cand_ind]
                                if post_ids[selection_index] == 1:
                                    ID_m[0] = TruthIDs_Bdbar[cand_ind]
                            if q2reg == "Sideband":
                                if B_m[0] < 4000. or B_m[0] > 5700.:
                                    events.Fill()
                            else:
                                events.Fill()
                        elif Kst_closer == False and DMs[selection_index] == True:
                            B_m[0] = m_B[cand_ind]
                            if truthIDs == True:
                                ID_m[0] = TruthIDs_Bd[cand_ind]
                            if q2reg == "Sideband":
                                if B_m[0] < 4000. or B_m[0] > 5700.:
                                    events.Fill()
                            else:
                                events.Fill()
                            B_m[0] = m_BBar[cand_ind]
                            if truthIDs == True:
                                ID_m[0] = TruthIDs_Bdbar[cand_ind]
                            if q2reg == "Sideband":
                                if B_m[0] < 4000. or B_m[0] > 5700.:
                                    events.Fill()
                            else:
                                events.Fill()

                event_counter += 1
                if breakdown == True:
                    No_cands_pre.Fill(OG_cands)
                    No_cands_post.Fill(post_candids)
                    if truthIDs == True:
                        No_truth_pre.Fill(Truth_OG_cands)
                        No_truth_post.Fill(Truth_post_candids)
                    event_tokens = np.add(event_tokens, event_counters).tolist()
                    if truthIDs == True:
                        Truth_event_tokens = np.add(Truth_event_tokens, Truth_event_counters)
                    # print Truth_OG_cands, Truth_event_counters

            elif channel == "muon":
                Dimuon_mass = getattr(tree, "BmumuKst_diMuon_mass")
                mu0_pt = getattr(tree, attr_names[0])
                mu1_pt = getattr(tree, attr_names[1])
                mu0_eta = getattr(tree, attr_names[2])
                mu1_eta = getattr(tree, attr_names[3])
                mes0_pt = getattr(tree, attr_names[4])
                mes1_pt = getattr(tree, attr_names[5])
                mes0_eta = getattr(tree, attr_names[6])
                mes1_eta = getattr(tree, attr_names[7])
                chi2_over_nDoF = getattr(tree, attr_names[8])
                m_K_Pi = getattr(tree, attr_names[9])
                m_Pi_K = getattr(tree, attr_names[10])
                tau_B = getattr(tree, attr_names[11])
                tau_B_err = getattr(tree, attr_names[12])
                tau_Bbar = getattr(tree, attr_names[13])
                tau_Bbar_err = getattr(tree, attr_names[14])
                m_B = getattr(tree, attr_names[15])
                m_BBar = getattr(tree, attr_names[16])
                Lxy = getattr(tree, attr_names[17])
                Lxy_err = getattr(tree, attr_names[18])
                #BmumuKst_diMuon_ID_OK = getattr(tree, attr_names[19]) Muon_ntuple not have this attribute?
                mu0_charge = getattr(tree, attr_names[20])
                mu1_charge = getattr(tree, attr_names[21])
                m0_charge = getattr(tree, attr_names[22])
                m1_charge = getattr(tree, attr_names[23])
                mu0_PL = getattr(tree, attr_names[24])
                mu1_PL = getattr(tree, attr_names[25])
                m0_PL = getattr(tree, attr_names[26])
                m1_PL = getattr(tree, attr_names[27])
                mu0_PLe = getattr(tree, attr_names[28])
                mu1_PLe = getattr(tree, attr_names[29])
                mu0_phi = getattr(tree, "BmumuKst_muon0_phi")
                mu1_phi = getattr(tree, "BmumuKst_muon1_phi")
                B_chi2 = getattr(tree, attr_names[32])
                dimuon_px = getattr(tree, attr_names[33])
                dimeson_px = getattr(tree, attr_names[34])
                mu0_iso_c40 = getattr(tree, attr_names[35])
                mu1_iso_c40 = getattr(tree, attr_names[36])
                B_pT = getattr(tree, attr_names[37])
                acc_trigger = getattr(tree, attr_names[38])

                vtx_BmumuKst_PV_minA0_x_err = getattr(tree, "BmumuKst_PV_minA0_x_err")
                vtx_BmumuKst_PV_minA0_y_err = getattr(tree, "BmumuKst_PV_minA0_y_err")
                vtx_BmumuKst_PV_minA0_z_err = getattr(tree, "BmumuKst_PV_minA0_z_err")
                vtx_BmumuKst_x_err = getattr(tree, "BmumuKst_x_err")
                vtx_BmumuKst_y_err = getattr(tree, "BmumuKst_y_err")
                vtx_BmumuKst_z_err = getattr(tree, "BmumuKst_z_err")
                vtx_BmumuKst_x = getattr(tree, "BmumuKst_x")
                vtx_BmumuKst_y = getattr(tree, "BmumuKst_y")
                vtx_BmumuKst_z = getattr(tree, "BmumuKst_z")
                vtx_BmumuKst_PV_minA0_x = getattr(tree, "BmumuKst_PV_minA0_x")
                vtx_BmumuKst_PV_minA0_y = getattr(tree, "BmumuKst_PV_minA0_y")
                vtx_BmumuKst_PV_minA0_z = getattr(tree, "BmumuKst_PV_minA0_z")
                mes0_phi = getattr(tree, "BmumuKst_meson0_phi")
                mes1_phi = getattr(tree, "BmumuKst_meson1_phi")

                if truthIDs == True and data_t == "MC":
                    if MCfile == "NR":
                        TruthIDs_Bd = getattr(tree, "BmumuKst_isTrueNonresBd")  # [0,0,1,1,0,0],
                        TruthIDs_Bdbar = getattr(tree, "BmumuKst_isTrueNonresBdbar")  # [0,0,0,0,0]
                    # print(TruthIDs_Bd)
                    elif MCfile == "JPsi":
                        TruthIDs_Bd = getattr(tree, "BmumuKst_isTrueResBd")  # [0,0,1,1,0,0],
                        TruthIDs_Bdbar = getattr(tree, "BmumuKst_isTrueResBdbar")  # [0,0,0,0,0]
                        # print(TruthIDs_Bdbar)
                if truthIDs == True:
                    Truth_event_counters = [0 for j in range(cuttingvarno)]
                    Truth_OG_cands = 0  # to be incremented in loop
                    Truth_post_candids = 0
                event_counters = [0 for j in range(cuttingvarno)]
                BBbar_event_counter = []
                OG_cands = len(mu0_pt)
                post_candids = 0  # No of candidates past cutflow in each event
                pass_cands = []  # list of the chi^2 of the post_cands and what they're indentified as, used to select them assigned IDs of the cands that pass the cutflow (0 - Bd, 1 -Bdbar)
                post_ids = []
                post_inds = []  # indices of the cands that pass the cutflow
                DMs= []

                post_Costhetas = [0 for cand in m_B]
                post_Costheta_errs = [0 for cand in m_B]

                BBar_mass_preselection = array('f', [0.])
                B_mass_preselection = array('f', [0.])
                BBar_mass_preselection_KCloser = array('f', [0.])
                B_mass_preselection_KCloser = array('f', [0.])

                #Prevent too much calculation for python
                q_1 = np.sqrt(1.1e6)
                q_2 = np.sqrt(6e6)
                q_3 = np.sqrt(11e6)
                q_4 = np.sqrt(16e6)
                q_5 = np.sqrt(49e6)

                B_or_Bbar_matched = False

                for ind, var in enumerate(acc_trigger):
                    if acc_trigger[ind] == "HLT_2mu6_bBmumuxv2_L1LFV-MU6":
                        for ind, var in enumerate(m_B):
                            counters[0] += 1
                            event_counters[0] = 1
                            if truthIDs == True:
                                if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                    Truth_counters[0] += 1
                                    Truth_event_counters[0] = 1
                            B_IDs = []
                            B_passed, K_passed, qual_passed = False, False, False
                            q2crit = False  # Set to True to remove q2 cuts

                            #q2 = (Dimuon_mass[ind] / 1000) ** 2
                            #q2 = Dimuon_mass[ind] * Dimuon_mass[ind]

                            if q2reg == "NR" and q_1 < Dimuon_mass[ind] < q_2:
                                q2crit = True
                            elif q2reg == "JPsi" and q_2 < Dimuon_mass[ind] < q_3:
                                q2crit = True
                            elif q2reg == "Psi2S" and q_3 < Dimuon_mass[ind] < q_4:
                                q2crit = True
                            elif q2reg == "Sideband" and q_1 < Dimuon_mass[ind] < q_2 and (m_B[ind] < 4000. or m_BBar[ind] < 4000. or m_B[ind] > 5700. or m_BBar[ind] > 5700.):
                                q2crit = True
                            if data_t == "data" and q2reg == "NR":
                                print("Probably shouldn't be doing that :p")
                                return

                            if q2crit == True :
                                counters[1] += 1
                                event_counters[1] = 1
                                if truthIDs == True:
                                    if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                        Truth_counters[1] += 1
                                        Truth_OG_cands += 1
                                        Truth_event_counters[1] = 1
                                if mu0_charge[ind] * mu1_charge[ind] < 0 and m0_charge[ind] * m1_charge[ind] < 0: #Ok
                                    counters[2] += 1
                                    event_counters[2] = 1
                                    if truthIDs == True:
                                        if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                            Truth_counters[2] += 1
                                            Truth_event_counters[2] = 1

                                    vtx_mu0p4 = ROOT.TLorentzVector()
                                    vtx_mu0p4.SetPtEtaPhiM(mu0_pt[ind], mu0_eta[ind], mu0_phi[ind], ml_config.PDG["mu_mass"])
                                    vtx_mu1p4 = ROOT.TLorentzVector()
                                    vtx_mu1p4.SetPtEtaPhiM(mu1_pt[ind], mu1_eta[ind], mu1_phi[ind], ml_config.PDG["mu_mass"])
                                    #vtx_dRmumu = vtx_mu0p4.DeltaR(vtx_mu1p4)
                                    vtx_dRmumu1 = np.sqrt((mu0_eta[ind]-mu1_eta[ind])*(mu0_eta[ind]-mu1_eta[ind])+(mu0_phi[ind]-mu1_phi[ind])*(mu0_phi[ind]-mu1_phi[ind]))
                                    if vtx_dRmumu1 >= 0.1:
                                        counters[3] += 1
                                        event_counters[3] = 1
                                        if truthIDs == True:
                                            if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                Truth_counters[3] += 1
                                                Truth_event_counters[3] = 1

                                        if mu0_pt[ind] >= muon_ptcut and mu1_pt[ind] >= muon_ptcut:
                                            counters[4] += 1
                                            event_counters[4] = 1
                                            if truthIDs == True:
                                                if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                    Truth_counters[4] += 1
                                                    Truth_event_counters[4] = 1
                                            if abs(mu0_eta[ind]) <= muon_etacut and abs(mu1_eta[ind]) <= muon_etacut:
                                                counters[5] += 1
                                                event_counters[5] = 1
                                                if truthIDs == True:
                                                    if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                        Truth_counters[5] += 1
                                                        Truth_event_counters[5] = 1
                                                if mes0_pt[ind] >= meson_ptcut and mes1_pt[ind] >= meson_ptcut:
                                                    counters[6] += 1
                                                    event_counters[6] = 1
                                                    if truthIDs == True:
                                                         if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                            Truth_counters[6] += 1
                                                            Truth_event_counters[6] = 1
                                                    if abs(mes0_eta[ind]) < meson_etacut and abs(mes1_eta[ind]) < meson_etacut:
                                                        counters[7] += 1
                                                        event_counters[7] = 1
                                                        if truthIDs == True:
                                                            if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                                Truth_counters[7] += 1
                                                                Truth_event_counters[7] = 1

                                                        if Dimuon_mass[ind] <= q_5:
                                                            counters[8] += 1
                                                            event_counters[8] = 1
                                                            if truthIDs == True:
                                                                if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                                    Truth_counters[8] += 1
                                                                    Truth_event_counters[8] = 1

                                                            #if LowBmass_cut <= m_B[ind] <= HighBmass_cut and LowBmass_cut <= m_BBar[ind] <= HighBmass_cut: ##### for "and" case
                                                            if LowBmass_cut <= m_B[ind] <= HighBmass_cut:
                                                                counters[9] += 1
                                                                event_counters[9] = 1
                                                                B_passed = True
                                                                if truthIDs:
                                                                    if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                                        Truth_counters[9] += 1
                                                                        Truth_event_counters[9] = 1
                                                                        Truth_post_candids += 1
                                                                #if Kst_cut_L <= m_K_Pi[ind] <= Kst_cut_H and Kst_cut_L <= m_Pi_K[ind] <= Kst_cut_H: ##### for "and" case
                                                                if Kst_cut_L <= m_K_Pi[ind] <= Kst_cut_H:
                                                                    K_passed = True
                                                                    counters[10] += 1
                                                                    event_counters[10] = 1
                                                                    if truthIDs:
                                                                        if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                                            Truth_counters[10] += 1
                                                                            Truth_event_counters[10] = 1

                                                                    if [mu0_PL[ind], mu1_PL[ind], m0_PL[ind], m1_PL[ind], mu0_PLe[ind], mu1_PLe[ind]] == [1, 1, 1, 1, 1, 1]:
                                                                        qual_passed = True
                                                                        counters[11] += 1
                                                                        event_counters[11] = 1
                                                                        if truthIDs:
                                                                            if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                                                Truth_counters[11] += 1
                                                                                Truth_event_counters[11] = 1

                                                                        if B_chi2[ind] < 100 :
                                                                            counters[15] += 1
                                                                            event_counters[15] = 1
                                                                            B_or_Bbar_matched = True

                                                                            if truthIDs:
                                                                                if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                                                    Truth_counters[15] += 1
                                                                                    Truth_event_counters[15] = 1
                                                                                    Truth_post_candids += 1

                                                            if LowBmass_cut <= m_BBar[ind] <= HighBmass_cut:
                                                                counters[12] += 1
                                                                event_counters[12] = 1
                                                                B_passed = True
                                                                if truthIDs:
                                                                    if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                                        Truth_counters[12] += 1
                                                                        Truth_event_counters[12] = 1
                                                                        Truth_post_candids += 1

                                                                if Kst_cut_L <= m_Pi_K[ind] <= Kst_cut_H:
                                                                    K_passed = True
                                                                    counters[13] += 1
                                                                    event_counters[13] = 1
                                                                    if truthIDs:
                                                                        if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                                            Truth_counters[13] += 1
                                                                            Truth_event_counters[13] = 1
                                                                    if [mu0_PL[ind], mu1_PL[ind], m0_PL[ind],m1_PL[ind], mu0_PLe[ind], mu1_PLe[ind]] == [1, 1, 1, 1, 1, 1]:
                                                                        qual_passed = True
                                                                        counters[14] += 1
                                                                        event_counters[14] = 1
                                                                        if truthIDs:
                                                                            if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                                                Truth_counters[14] += 1
                                                                                Truth_event_counters[14] = 1

                                                                        if B_chi2[ind] < 100 :
                                                                            counters[15] += 1
                                                                            event_counters[15] = 1
                                                                            B_or_Bbar_matched = True

                                                                            if truthIDs:
                                                                                if TruthIDs_Bd[ind] == 1 or TruthIDs_Bdbar[ind] == 1:
                                                                                    Truth_counters[15] += 1
                                                                                    Truth_event_counters[15] = 1
                                                                                    Truth_post_candids += 1

                                                                    if B_or_Bbar_matched == True:
                                                                        tau_B_over_err = tau_B[ind] / tau_B_err[ind]
                                                                        Lxy_B_over_err = Lxy[ind] / Lxy_err[ind]
                                                                        # -----------------------------Defining CosTheta------------------------------
                                                                        vtx_Bd_vtx = ROOT.TVector3()
                                                                        vtx_Bd_vtx.SetXYZ(vtx_BmumuKst_x[ind], vtx_BmumuKst_y[ind], vtx_BmumuKst_z[ind])
                                                                        vtx_Bd_pv = ROOT.TVector3()
                                                                        vtx_Bd_pv.SetXYZ(vtx_BmumuKst_PV_minA0_x[ind],vtx_BmumuKst_PV_minA0_y[ind], vtx_BmumuKst_PV_minA0_z[ind])
                                                                        vtx_pv2B = vtx_Bd_vtx - vtx_Bd_pv

                                                                        vtx_mu0p4 = ROOT.TLorentzVector()
                                                                        vtx_mu0p4.SetPtEtaPhiM(mu0_pt[ind], mu0_eta[ind], mu0_phi[ind], ml_config.PDG["mu_mass"])
                                                                        vtx_mu1p4 = ROOT.TLorentzVector()
                                                                        vtx_mu1p4.SetPtEtaPhiM(mu1_pt[ind], mu1_eta[ind], mu1_phi[ind], ml_config.PDG["mu_mass"])
                                                                        vtx_diMuon_p4 = vtx_mu0p4 + vtx_mu1p4

                                                                        vtx_m0_pi_p4 = ROOT.TLorentzVector()
                                                                        vtx_m0_pi_p4.SetPtEtaPhiM(mes0_pt[ind], mes0_eta[ind], mes0_phi[ind], ml_config.PDG["pipm_mass"])
                                                                        vtx_m0_K_p4 = ROOT.TLorentzVector()
                                                                        vtx_m0_K_p4.SetPtEtaPhiM(mes0_pt[ind], mes0_eta[ind], mes0_phi[ind], ml_config.PDG["Kpm_mass"])
                                                                        vtx_m1_pi_p4 = ROOT.TLorentzVector()
                                                                        vtx_m1_pi_p4.SetPtEtaPhiM(mes1_pt[ind], mes1_eta[ind], mes1_phi[ind], ml_config.PDG["pipm_mass"])
                                                                        vtx_m1_K_p4 = ROOT.TLorentzVector()
                                                                        vtx_m1_K_p4.SetPtEtaPhiM(mes1_pt[ind], mes1_eta[ind], mes1_phi[ind], ml_config.PDG["Kpm_mass"])

                                                                        vtx_Kpi_p4 = vtx_m0_K_p4 + vtx_m1_pi_p4
                                                                        vtx_piK_p4 = vtx_m0_pi_p4 + vtx_m1_K_p4
                                                                        vtx_Bd_p4 = vtx_diMuon_p4 + vtx_Kpi_p4
                                                                        vtx_Bdbar_p4 = vtx_diMuon_p4 + vtx_piK_p4

                                                                        vtx_Bd_p3 = vtx_Bd_p4.Vect()
                                                                        vtx_Bd_p3_err = ROOT.TVector3()
                                                                        vtx_Bd_p3_err.SetXYZ(0., 0., 0.)
                                                                        vtx_Bd_pv_err = ROOT.TVector3()
                                                                        vtx_Bd_pv_err.SetXYZ(vtx_BmumuKst_PV_minA0_x_err[ind], vtx_BmumuKst_PV_minA0_y_err[ind], vtx_BmumuKst_PV_minA0_z_err[ind])

                                                                        vtx_Bd_vtx_err = ROOT.TVector3()
                                                                        vtx_Bd_vtx_err.SetXYZ(vtx_BmumuKst_x_err[ind], vtx_BmumuKst_y_err[ind], vtx_BmumuKst_z_err[ind])
                                                                        vtx_pv2B_err = ROOT.TVector3()
                                                                        vtx_pv2B_err.SetXYZ(np.sqrt(vtx_Bd_vtx_err.X() * vtx_Bd_vtx_err.X() + vtx_Bd_pv_err.X() * vtx_Bd_pv_err.X()), np.sqrt(vtx_Bd_vtx_err.Y() * vtx_Bd_vtx_err.Y() + vtx_Bd_pv_err.Y() * vtx_Bd_pv_err.Y()), np.sqrt(vtx_Bd_vtx_err.Z() * vtx_Bd_vtx_err.Z() + vtx_Bd_pv_err.Z() * vtx_Bd_pv_err.Z()))
                                                                        BmumuKst_CosTheta = vtx_pv2B.Dot(vtx_Bd_p3) / np.sqrt(vtx_pv2B.Dot(vtx_pv2B) * vtx_Bd_p3.Dot(vtx_Bd_p3))
                                                                        BmumuKst_CosTheta_err = cos_Theta_error(vtx_pv2B, vtx_pv2B_err, vtx_Bd_p3, vtx_Bd_p3_err)
                                                                        post_Costhetas[ind] = BmumuKst_CosTheta
                                                                        post_Costheta_errs[ind] = BmumuKst_CosTheta / BmumuKst_CosTheta_err

                                                                        # ----------------------------------------
                                                                        if select_bestcand:
                                                                            Dimuon_m[0] = Dimuon_mass[ind]
                                                                            mu0_pt_m[0] = mu0_pt[ind]
                                                                            mu1_pt_m[0] = mu1_pt[ind]
                                                                            mu0_eta_m[0] = mu0_eta[ind]
                                                                            mu1_eta_m[0] = mu1_eta[ind]
                                                                            mes0_pt_m[0] = mes0_pt[ind]
                                                                            mes1_pt_m[0] = mes1_pt[ind]
                                                                            mes0_eta_m[0] = mes0_eta[ind]
                                                                            mes1_eta_m[0] = mes1_eta[ind]
                                                                            tau_m[0] = tau_B[ind]
                                                                            tau_over_err[0] = tau_B_over_err
                                                                            Lxy_m[0] = Lxy[ind]
                                                                            Lxy_over_err[0] = Lxy_B_over_err
                                                                            mu0_phi_m[0] = mu0_phi[ind]
                                                                            mu1_phi_m[0] = mu1_phi[ind]
                                                                            B_chi2_m[0] = B_chi2[ind]
                                                                            dimuon_px_m[0] = dimuon_px[ind]
                                                                            dimeson_px_m[0] = dimeson_px[ind]
                                                                            mu0_iso_c40_m[0] = mu0_iso_c40[ind]
                                                                            mu1_iso_c40_m[0] = mu1_iso_c40[ind]
                                                                            B_pT_m[0] = B_pT[ind]
                                                                            cos_theta[0] = BmumuKst_CosTheta
                                                                            cos_theta_err[0] = BmumuKst_CosTheta / BmumuKst_CosTheta_err
                                                                            for B_id in B_IDs:
                                                                                if B_id == 0:
                                                                                    B_m[0] = m_B[ind]
                                                                                if B_id == 1:
                                                                                    B_m[0] = m_BBar[ind]
                                                                                if truthIDs:
                                                                                    if B_id == 0:
                                                                                        ID_m[0] = TruthIDs_Bd[ind]
                                                                                    if B_id == 1:
                                                                                        ID_m[0] = TruthIDs_Bdbar[ind]
                                                                                if q2reg == "Sideband":
                                                                                    if B_m[0] < 4000. or B_m[0] > 5700.:
                                                                                        events.Fill()
                                                                                else:
                                                                                    events.Fill()

                #----------Event basis------------- #Chooses between multiple Bd or Bdbar candidates in an event
                if select_bestcand == True:
                    if pass_cands != []:
                        # print("Passed candidate chi2/ndofs", pass_cands)
                        # index of passed cand with lowest chi2 [pass1_B , pass1_Bbar, .... ]
                        selection_index = np.argmin(pass_cands)
                        # print("highest cand ind:", selection_index)
                        # the index in the event list of candidates that corresponds to lowest chi2 [cand1, cand2,...]
                        cand_ind = post_inds[selection_index]
                        # print("post indexes", post_inds)
                        # print("cand ind:", cand_ind)
                        # print("DMs:", DMs)
                        #counters[17] += 1
                        #event_counters[17] = 1
                        #q2 = (Dimuon_mass[cand_ind] / 1000) ** 2
                        #if truthIDs == True:
                        #    if TruthIDs_Bdbar[cand_ind] == 1 or TruthIDs_Bd[cand_ind] == 1:
                        #        Truth_counters[17] += 1
                        #       Truth_event_counters[17] = 1
                        mu0_pt_m[0] = mu0_pt[cand_ind]
                        mu1_pt_m[0] = mu1_pt[cand_ind]
                        mu0_eta_m[0] = mu0_eta[cand_ind]
                        mu1_eta_m[0] = mu1_eta[cand_ind]
                        mes0_pt_m[0] = mes0_pt[cand_ind]
                        mes1_pt_m[0] = mes1_pt[cand_ind]
                        mes0_eta_m[0] = mes0_eta[cand_ind]
                        mes1_eta_m[0] = mes1_eta[cand_ind]
                        Dimuon_m[0] = Dimuon_mass[cand_ind]

                        tau_B_over_err = tau_B[cand_ind] / tau_B_err[cand_ind]
                        Lxy_B_over_err = Lxy[cand_ind] / Lxy_err[cand_ind]
                        tau_m[0] = tau_B[cand_ind]
                        tau_over_err[0] = tau_B_over_err
                        Lxy_m[0] = Lxy[cand_ind]
                        Lxy_over_err[0] = Lxy_B_over_err
                        cos_theta[0] = post_Costhetas[cand_ind]
                        cos_theta_err[0] = post_Costheta_errs[cand_ind]

                        mu0_phi_m[0] = mu0_phi[cand_ind]
                        mu1_phi_m[0] = mu1_phi[cand_ind]
                        B_chi2_m[0] = B_chi2[cand_ind]
                        dimuon_px_m[0] = dimuon_px[cand_ind]
                        dimeson_px_m[0] = dimeson_px[cand_ind]
                        mu0_iso_c40_m[0] = mu0_iso_c40[cand_ind]
                        mu1_iso_c40_m[0] = mu1_iso_c40[cand_ind]
                        B_pT_m[0] = B_pT[cand_ind]
                        if Kst_closer == True or DMs[selection_index] == False:
                            if post_ids[selection_index] == 0:
                                B_m[0] = m_B[cand_ind]
                            if post_ids[selection_index] == 1:
                                B_m[0] = m_BBar[cand_ind]
                            if truthIDs == True:
                                if post_ids[selection_index] == 0:
                                    ID_m[0] = TruthIDs_Bd[cand_ind]
                                if post_ids[selection_index] == 1:
                                    ID_m[0] = TruthIDs_Bdbar[cand_ind]
                            if q2reg == "Sideband":
                                if B_m[0] < 4000. or B_m[0] > 5700.:
                                    events.Fill()
                            else:
                                events.Fill()
                        elif Kst_closer == False and DMs[selection_index] == True:
                            B_m[0] = m_B[cand_ind]
                            if truthIDs == True:
                                ID_m[0] = TruthIDs_Bd[cand_ind]
                            if q2reg == "Sideband":
                                if B_m[0] < 4000. or B_m[0] > 5700.:
                                    events.Fill()
                            else:
                                events.Fill()
                            B_m[0] = m_BBar[cand_ind]
                            if truthIDs == True:
                                ID_m[0] = TruthIDs_Bdbar[cand_ind]
                            if q2reg == "Sideband":
                                if B_m[0] < 4000. or B_m[0] > 5700.:
                                    events.Fill()
                            else:
                                events.Fill()

                event_counter += 1

                if breakdown == True: #manually change to false
                    No_cands_pre.Fill(OG_cands)
                    No_cands_post.Fill(post_candids)
                    if truthIDs == True and data_t == "MC":
                        No_truth_pre.Fill(Truth_OG_cands)
                        No_truth_post.Fill(Truth_post_candids)
                    event_tokens = np.add(event_tokens, event_counters).tolist()
                    if truthIDs == True:
                        Truth_event_tokens = np.add(Truth_event_tokens, Truth_event_counters)
                    # print Truth_OG_cands, Truth_event_counters
    events.Write("", ROOT.TObject.kOverwrite)

    if breakdown == True:
        # print (double_matched, " double matched")
        # print('---------------------------------------------------')
        print(counters)
        print(event_tokens)
        if truthIDs == True:
            print(Truth_counters)
            print(Truth_event_tokens)

        rel_eff = [float(event_tokens[i]) / event_tokens[i - 1] for i in range(1,len(event_tokens))] 
        print("rel_eff", rel_eff)
        if truthIDs == True:
            rel_eff_T = [Truth_event_tokens[i] / np.float(Truth_event_tokens[i - 1]) for i in range(1, len(Truth_event_tokens))]
            purities = [float(Truth_event_tokens[i]) / event_tokens[i] for i in range(len(event_tokens))]
            eff_rat = [rel_eff_T[i] / rel_eff[i] for i in range(len(rel_eff))]
            print("rel_eff_T", rel_eff_T)
            print("purities", purities)
            print("eff_rat", eff_rat)

        #mytable = PrettyTable(["Selection Critertion", "Candidates", "Events", "Truth-matched candidates", "Truth-mathced events", "Relative Efficiency", "Relative Efficiency(truth-matched)", "Purity", "Ratio of relative efficiencies"])
        mytable = PrettyTable()

        mytable.title = 'Cutflow table for Non-Resonance MC sample (Muon)'
        mytable.field_names = ["highq2MC_𝜇_Critertion", "Candidates", "Events", "Truth_Cand", "Truth_Events", "Rel_Eff", "Rel_Eff_truth", "Purity", "Ratio of rel_eff"]

        criteria = ["HLT_Trigger", "baseline", "Q(l1,mes1)*Q(l2,mes2)<0", "ΔR𝜇𝜇≥0.1","pT(l1/2)≥6GeV", "∣𝜂(l1/2)∣≤2.5", "pT(meson1/2)≥0.5GeV", "∣𝜂(meson1/2)∣≤2.5", "m(𝜇𝜇)≤7GeV","𝑚(𝐾𝜋𝜇𝜇∈[4700,6000]𝑀𝑒𝑉)", "𝑚(𝐾𝜋∈[690,1110]𝑀𝑒𝑉)", "𝜇 ID requirements","𝑚(𝜋𝐾𝜇𝜇∈[4700,6000]𝑀𝑒𝑉)", "𝑚(𝜋𝐾∈[690,1110]𝑀𝑒𝑉)", "𝜇 ID requirements","best chi2/dof<100"]#"Track Quality & 𝜇 ID requirements"
        #TriggerSelection(HLT_2mu6_bBmumuxv2_L1LFV - MU6

        rel_eff.insert(0,"NAN")
        rel_eff_T.insert(0,"NAN")
        eff_rat.insert(0,"NAN")

        rel_eff_12 = float(event_tokens[12] / np.float(event_tokens[8]))
        rel_eff_T_12 = float(Truth_event_tokens[12] / np.float(Truth_event_tokens[8]))
        rel_eff_15 = float(event_tokens[15]/ np.float(event_tokens[11]+event_tokens[14]))
        rel_eff_T_15 = float(Truth_event_tokens[15] / np.float(Truth_event_tokens[11]+Truth_event_tokens[14]))
        rel_eff[12] = rel_eff_12
        rel_eff_T[12] = rel_eff_T_12
        rel_eff[15] = rel_eff_15
        rel_eff_T[15] = rel_eff_T_15

        #len(counters)
        for i in range(len(counters)):
            mytable.add_row([criteria[i], counters[i], event_tokens[i], Truth_counters[i], Truth_event_tokens[i], rel_eff[i], rel_eff_T[i], purities[i], eff_rat[i]])

        print(mytable)

        if truthIDs == True:
            breakdowndata = np.asarray([counters,event_tokens,Truth_counters,Truth_event_tokens,rel_eff,rel_eff_T,purities,eff_rat])
        else:
            breakdowndata = np.asarray([np.asarray(counters), np.asarray(event_tokens), np.asarray(rel_eff)])
        #print(breakdowndata)
        #if data_t == "MC":
        #    with open("{}/cutflow{}{}_{}_CutflowChallenge.csv".format(MCfile ,data_t, MCfile, q2reg), "wb") as cut:
        #        writer = csv.writer(cut)
        #        writer.writerows(breakdowndata)
        #else:
        #    with open("{}/cutflow{}_{}_CutflowChallenge.csv".format(data_t, data_t, q2reg), "wb") as cut:
        #        writer = csv.writer(cut)
        #        writer.writerows(breakdowndata)

    Draw_PreselectionHists = True
    if Draw_PreselectionHists:
        from ROOT import TFile
        c1 = ROOT.TCanvas('B_VTX_Mass', 'Events', 1000, 300)
        c1.Divide(4)
        c1.cd(1)
        No_cands_pre.Draw("hist")
        c1.cd(2)
        No_cands_post.Draw("hist")
        if truthIDs:
            if data_t == "MC":
                c1.cd(3)
                No_truth_pre.Draw("hist")
                c1.cd(4)
                No_truth_post.Draw("hist")
                #c1.SaveAs("CutFlow_cand_counts_MC_{}{}_CutflowChallenge".format(q2reg, MCfile))
                # Save to your new file name
                f.WriteObject(c1,"c1")
                f.WriteObject(No_truth_pre, "No_truth_pre")
                f.WriteObject(No_truth_post, "No_truth_post")
            elif data_t == "data":
                #c1.SaveAs("CutFlow_cand_counts_Data_{}_CutflowChallenge".format(q2reg))
                # Save to your new file name
                f.WriteObject(c1,"c1")

        #c1.SaveAs("CutFlow_cand_counts_MC_{}{}_CutflowChallenge".format(q2reg, MCfile))
        #Save to your new file name
        f.WriteObject(No_cands_pre, "No_cands_pre")
        f.WriteObject(No_cands_post, "No_cands_post")

    if truthIDs:
        return Truth_event_tokens
    else:
        return event_tokens

# 4. Run the Program
#tree_files = "data_tree_files"/"MC_NR_tree_files"/"MC_JPsi_tree_files"/"MC_Psi2S_tree_files"
FlowCutter(MC_JPsi_tree_files, q2reg, True, True)


# 5. Extra Material can put in here
#if Rootfile == "data":
    #periods = ['L']  # ['F','I', 'O','K', 'Q', 'M', 'L']
    #for ind, period in enumerate(periods):
        #data_filenm = data_filenms[period]
        #print(data_filenm)
        #data_files = [ROOT.TFile(name) for name in data_filenm]
        #data_files[0].Print()
        #data_tree_files = [data_files[0].Get("Nominal/BaseSelection_KStaree_BeeKstSelection")] #swapping out just to get L part01[
        #data_tree_files = [dfile.Get("Nominal/BaseSelection_KStaree_BeeKstSelection") for dfile in data_files]
        #FlowCutter(data_tree_files,"Sideband",True, False)
        #FlowCutter(data_tree_files,"Psi2S",False, False)


#FlowCutter(data_tree_files,"Sideband",True, False)
#FlowCutter(data_tree_files,"JPsi",True, True)
#FlowCutter(MC_NR_tree_files, "NR", True, True)
#FlowCutter(MC_JPsi_tree_files,"JPsi",True,True)
#FlowCutter(MC_Psi2S_tree_files,"Psi2S",False, False)
#FlowCutter(MC_Background_tree_files1, "NR", True, False)
#FlowCutter(MC_Background_tree_files2, "NR", True, False)
#FlowCutter(MC_Background_tree_files3, "NR", True, False)
