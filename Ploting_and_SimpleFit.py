﻿import numpy as np
import ROOT
from array import array
from ROOT import TMath, TCanvas, TH1, TF1, gStyle
import math

Canvas_property = False #Declare Canvas Property, showing chi square value after fitting
Histogram_Declare = True #Declare Histogram if you collect Data from Tree
Data_Collect_tree = True #Collect Data from Tree
Data_Collect_histogram = False #Collect histogram
Fitting = False #Using TH1::Fit method
Plotting = False #Plotting without fitting

##-----------------------------------------------------------------------------------------
#Part 1 Declare the histograms and Canvas
##Canvas Property
if Canvas_property == True:
    gStyle.SetOptFit(1111)
    gStyle.SetOptStat(0)
    gStyle.SetStatX(0.97)
    gStyle.SetStatY(0.90)
    gStyle.SetStatH(0.15)
    gStyle.SetStatW(0.13)
    TH1.SetDefaultSumw2()
##Histogram
if Histogram_Declare == True:
    h0 = ROOT.TH1D("h0", "B mass using K_closer after preselection", 100, 4700, 5700)
    h0.SetStats(1)
    h0.SetMarkerStyle(10)
    h0.SetMarkerSize(0.5)
    h0.SetMinimum(0)
    h2 = ROOT.TH1D("h2", "B and B bar mass after preselection", 100, 4700, 5700)
    h2.SetStats(1)
    h2.SetMarkerStyle(10)
    h2.SetMarkerSize(0.5)
    h2.SetMinimum(0)

##----------------------------------------------------------------------------------------
#Part 2 import your data
if Data_Collect_tree == True:
    #DataFile = ROOT.TFile.Open["/afs/cern.ch/user/c/cklo/public/TestingData.root"]
    rootfile = "/afs/cern.ch/user/c/cklo/public/TestingData.root"
    DataFile = ROOT.TFile(rootfile)
    ROOT.gDirectory.pwd()
    tree = ROOT.gDirectory.Get("tree")
    attr_names = ["BmumuKst_B_mass"]
    n_events = tree.GetEntries()
    print(n_events)
    LC = 4000
    for i in range(n_events): #n_events): #n_events
        tree.GetEntry(i) #access event i
        mass = getattr(tree, attr_names[0])
        if LC < mass < 6500:
            h2.Fill(mass) #Fill B_mass to your histogram h2
    Save_Data = True
    if Save_Data == True:
        f = ROOT.TFile("Bmass_Histogram.root", "RECREATE")
        f.WriteObject(h2,"h2")

if Data_Collect_histogram == True:
    #DataFile = ROOT.TFile["/afs/cern.ch/user/c/cklo/public/TestingData.root"]
    rootfile1 = "/afs/cern.ch/user/c/cklo/public/TestingData.root"
    DataFile = ROOT.TFile(rootfile1)
    ROOT.gDirectory.pwd()
    h1 = ROOT.gDirectory.Get("h1")

##------------------------------------------------------------------------------------------
##Part 3 Fitting
if Fitting == True:
    # Apply Fitting (For good fitting, lower chi square as much as possible)
    # Default in TF1::Fit polN, expo, gaus, landau, can try others
    h1.Fit("gaus")

##-------------------------------------------------------------------------------------------
#Part 4 Plotting
if Plotting == True:
    Data_Canvas = TCanvas ("c1","c1", 1500, 500)
    c1.Divide(2,1)
    c1.cd(1)
    h0.Draw()
    c1.cd(2)
    h1.Draw()
    c1.Draw()

