import numpy as np
import ROOT
from array import array

#-----------------------Accessing Root files---------------------------------
def createCanvasPads():
    c1 =  ROOT.TCanvas('B_Vertex_Mass','Events', 800, 800)
    # Upper histogram plot is pad1
    pad1 = ROOT.TPad("pad1", "pad1", 0, 0.3, 1, 1.0) #xmin, ymin, xmax, ymax
    pad1.SetBottomMargin(0)  # joins upper and lower plot
    pad1.SetGridx()
    pad1.Draw()
    # Lower ratio plot is pad2
    c1.cd()  # returns to main canvas before defining pad2
    pad2 = ROOT.TPad("B_Vertex_Mass", "B_Vertex_Mass", 0, 0, 1, 0.3)
    pad2.SetTopMargin(0)  # joins upper and lower plot
    pad2.SetBottomMargin(0.2)
    pad2.SetGridx()
    pad2.Draw()

    return c1, pad1, pad2
#c1, pad1, pad2 = createCanvasPads()

def SidebandPlotter(TFile_no, LC):
    #-------------------------defining ROOT variables--------------------
    filenms = ["data_sideband"]
    filedict = {
    filenms[0]:"/afs/cern.ch/user/c/cklo/public/TestingData_Sideband_JPsi.root",
    }
    file1 = ROOT.TFile.Open(filedict[filenms[TFile_no]]) #open 1st file
    ROOT.gDirectory.pwd() #work in the open file
    tree= ROOT.gDirectory.Get("tree")

    attr_names = ["BmumuKst_B_mass"]

    mass         = np.zeros(1,dtype=float)
    numEntry     = tree.GetEntries()
    w = ROOT.RooWorkspace("w")
    m = ROOT.RooRealVar('m', 'm', LC, 5800.)
    m_arg        = ROOT.RooArgSet(m)
    w.Import(m_arg)
    data         = ROOT.RooDataSet("data", "data", m_arg)

    data.Print("v")
    #-------------------------Accessing data-----------------
    n_events = tree.GetEntries()
    print(n_events)
    for i in range(n_events): #n_events): #n_events
        tree.GetEntry(i) #access event i
        mass =  getattr(tree,attr_names[0])
        if LC < mass < 5800:
            ROOT.RooAbsRealLValue.__assign__(m, mass)
            data.add(m_arg,1.0)
    data.Print("v")
    filename = ""
    #-----------------Fitting---------------------
    m = w.var('m')
    m.setRange("LSB", LC,4900.)
    m.setRange("HSB", 5600.,5800.)
    #w.factory("RooCBShape::signal_pdf(m, mu[4000,3500,5000],  sig1[50,0,300], alpha1[0.33759,0,10], n1[2.14430,0,10])") #, alphaR[2,0,10], nR[5,0,500])")
    #w.factory("Gaussian:signal2(m, mu, sig2[202.72,150,350])")
    w.factory("Gaussian:signal_pdf(m, mu[4000,3700,6500], sig2[240.,150, 500])")
    #w.factory("RooExponential:signal_pdf(m, c1[-35e-5, -1e-2, 1e-6])")
    w.factory("RooExponential:bkg_pdf(m, c2[-35e-5, -1e-2, 1e-6])")
    #w.factory("RSUM:signal_pdf(f1[0.8050,1]*signal1,signal2)")
    w.factory("SUM:model(No_sig[8000,0,{}]*signal_pdf, No_bck[80000,0,{}]*bkg_pdf)".format(n_events*2, n_events*2))
    #pdf = w.pdf('bkg_pdf')
    pdf = w.pdf('model')
    """
    #w.factory("Gaussian:signal2(m, mu, sigma2[140,0,500])")
    w.factory("RooCBShape::signal1(m, mu[5262.1,5200,5300],  sig1[89.3,80,90], alpha1[0.41,0.01,10], n1[1.6,0.01,10])") #, alphaR[2,0,10], nR[5,0,500])")
    w.factory("Gaussian:signal2(m, mu, sig2[258,0,300])")
    #w.factory("Gaussian:signal3(m, mu, sig3[25, 0, 50])")
    #w.factory("RooCBShape::signal2(m, mu, sigma2[140,0,500], alpha2[1.5,0,100], n2[2.5,0,500])")

    w.factory("RooExponential:bkg_pdf(m, c1[-0.,0,0.])")
    #.factory("Roo[])")
    #w.factory("RooExponential:bkg2(m, c2[-0.004593,-6e-3,-1e-3])")
    #w.factory("RSUM:bkg_pdf(f2[0.5,0.0,1]*bkg1,bkg2)")
    #w.factory("Gaussian::bkg(m, mu, sigma3[500,0,1000])")

    w.factory("RSUM:signal_pdf(f1[0.835,0,1]*signal1,signal2)")
    #w.factory("RSUM:signal_pdf(f2[0.172]*signal_t,signal3)")
    #.factory("RSUM:bkg_pdf(f2[0.5,0,1]*bkg1, bkg2)")  No_sig[9453,0 ,45000]

    w.factory("SUM:model(No_sig[30000,0,200000]*signal_pdf, No_bck[0,0,1000]*bkg_pdf)")
    """
    pdf.fitTo(data, ROOT.RooFit.PrintLevel(-1),  ROOT.RooFit.Range("LSB,HSB"))#, ROOT.RooFit.Range("3500,6500"))
    normSet = ROOT.RooFit.NormSet(m_arg)
    m.setRange("signal", 4900., 5600.)
    reg2 = pdf.createIntegral(m_arg,normSet,ROOT.RooFit.Range("signal")).getVal()
    reg1 = pdf.createIntegral(m_arg,normSet,ROOT.RooFit.Range("LSB")).getVal()
    reg3 = pdf.createIntegral(m_arg,normSet,ROOT.RooFit.Range("HSB")).getVal()
    print(reg1, reg2,reg3)
    #------------------Create pads for plots--------------
    #------------------plotting--------------------
    c1, pad1, pad2 = createCanvasPads()
    pad1.cd()  #upper pad
    pl = m.frame(ROOT.RooFit.Title("B_Meson_Mass"))
    data.plotOn(pl)
    pdf.plotOn(pl, ROOT.RooFit.Components("signal_pdf")) #, ROOT.RooFit.LineStyle(2))
    #pdf.plotOn(pl, ROOT.RooFit.Components("model"), ROOT.RooFit.LineStyle(2))
    #pdf.plotOn(pl, ROOT.RooFit.Components("signal2"), ROOT.RooFit.LineStyle(2))
    pdf.plotOn(pl, ROOT.RooFit.Components("bkg_pdf"), ROOT.RooFit.LineColor(2), ROOT.RooFit.LineStyle(2))
    pdf.paramOn(pl, ROOT.RooFit.Layout(0.4,0.6,0.9))
    pdf.plotOn(pl)
    pl.GetXaxis().SetRangeUser(LC, 5800.)
    #pl.GetYaxis().SetRangeUser(0., 1500.)
    pl.SetMinimum(0.01)
    #pl.GetYaxis().SetRangeUser(0.01, 1000.)
    pl.SetTitle("")
    pl.Draw()  #draw on the current pad
    pad2.cd()  #lowerpad

    pullhist = pl.pullHist()
    pl2 = m.frame(ROOT.RooFit.Title(""))
    pl2.addPlotable(pullhist, "P")


    pl2.GetXaxis().SetNdivisions(207)
    pl2.GetYaxis().SetNdivisions(207)
    pl2.GetYaxis().SetLabelSize(0.07)

    pl2.GetYaxis().SetRangeUser(-5., 5.)
    pl2.GetXaxis().SetTitleSize(0.10)
    pl2.GetXaxis().SetLabelSize(0.07)
    #residhist.SetMaximum(9.99)

    pl2.SetTitle("")
    pl2.GetXaxis().SetTitle("B_Vertex_Mass [MeV]")
    pl2.GetYaxis().SetTitle("Pull")
    pl2.GetYaxis().SetTitleSize(0.09)
    pl2.GetYaxis().SetTitleOffset(0.3)

    pl2.Draw()

    line = ROOT.TLine(LC,0,5800.,0)
    line.SetLineColor(ROOT.kRed)
    line.SetLineWidth(2)
    line.Draw("same")

    c1.Draw()
    c1.SaveAs("{}.png".format(filenms[TFile_no]))
    return reg1, reg2, reg3
SidebandPlotter(0, 4700.)

