#!/usr/bin/env python3

# MC IDs
# ======

electron_nr_signal_Bd_id    = ["300590"]
electron_nr_signal_BdBar_id = ["300591"]
electron_r_signal_Bd_id     = ["300592"]
electron_r_signal_BdBar_id  = ["300593"]
electron_nr_signal_id = electron_nr_signal_Bd_id + electron_nr_signal_BdBar_id
electron_r_signal_id  = electron_r_signal_Bd_id  + electron_r_signal_BdBar_id
electron_signal_Bd_id    = electron_nr_signal_Bd_id    + electron_r_signal_Bd_id
electron_signal_BdBar_id = electron_nr_signal_BdBar_id + electron_r_signal_BdBar_id
electron_signal_id = electron_nr_signal_id + electron_r_signal_id

muon_nr_signal_Bd_id    = ["300700"]
muon_nr_signal_BdBar_id = ["300701"]
muon_r_signal_Bd_id     = ["300702"]
muon_r_signal_BdBar_id  = ["300703"]
muon_nr_signal_id = muon_nr_signal_Bd_id + muon_nr_signal_BdBar_id
muon_r_signal_id  = muon_r_signal_Bd_id  + muon_r_signal_BdBar_id
muon_signal_Bd_id    = muon_nr_signal_Bd_id    + muon_r_signal_Bd_id
muon_signal_BdBar_id = muon_nr_signal_BdBar_id + muon_r_signal_BdBar_id
muon_signal_id = muon_nr_signal_id + muon_r_signal_id

# Constants
# =========
PDG = {
    "Jpsi_mass"  : 3096.90, # MeV
    "e_mass"     : 0.511,   # MeV
    "mu_mass"     : 105.658,   # MeV
    "pipm_mass"  : 139.57,  # MeV
    "Kpm_mass"   : 493.677, # MeV
    "Kstar_mass" : 891.66,  # MeV
    "Bd_mass"    : 5279.64, # MeV
    "Bd_tau"     : 1.520,   # ps
}



# TODO: already in ml_share/samples.py
BRs = {
    "non-resonant_Bd" : 0.00000103, # to K* only, no K*->Kpi!
    "resonant_Bd"     : 0.00127 * 0.0594, # to K* only, no K*->Kpi!
}

# AOD info
# ========
# TODO: already in ml_share/samples.py
N_AOD = {
    "muon_non-resonant_Bd"    : 1498000,
    "muon_non-resonant_BdBar" : 1499000,
    "muon_resonant_Bd"        : 436000,
    "muon_resonant_BdBar"     : 449500,
    "electron_non-resonant_Bd"    : 6441500,
    "electron_non-resonant_BdBar" : 4976000,
    "electron_resonant_Bd"        : 447500,
    "electron_resonant_BdBar"     : 449500,
}

gen_eff = {
    "muon_non-resonant_Bd"    : 0.064422,
    "muon_non-resonant_BdBar" : 0.064026,
    "muon_resonant_Bd"        : 0.061365,
    "muon_resonant_BdBar"     : 0.060837,
    "electron_non-resonant_Bd"    : 0.05335,
    "electron_non-resonant_BdBar" : 0.052891,
    "electron_resonant_Bd"        : 0.058504,
    "electron_resonant_BdBar"     : 0.058024,
}



# Aliases
# =======

aliases = {
    "event_number"      : "info_event_number",
    "sample_id"         : "info_sample",
    "is_true"           : "info_truth_matching",
    "B_chi2"            : "B_chi2",
    "B_mass_closer"     : "B_Bbar_mass_closer_PDG",
    "Kstar_mass_closer" : "diMeson_Kpi_piK_mass_closer_PDG",
}

vetoes = {
    "phi_veto"   : '( (df["diMeson_KK_mass"]<1001.09) | (df["diMeson_KK_mass"]>1038.49) )',
    "muon_psi2S_veto" : '(df["diMuon_mass"]*df["diMuon_mass"]<11e6)',
    "electron_psi2S_veto" : '(df["diElectron_mass"]*df["diElectron_mass"]<11e6)',
}
