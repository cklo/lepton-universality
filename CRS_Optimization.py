import numpy as np
import ROOT
from ROOT import RooFit
from array import array
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams['text.usetex']=True
matplotlib.rcParams['text.latex.unicode']=True

def CutOptimiser(MCfile, Datafile, attr1, attr2, bkgFirst1,bkgFirst2,LC1,LC2, HC1,HC2,cuts1,cuts2):
    TFileMC   = ROOT.TFile(MCfile)
    TTreeMC   = TFileMC.Get("tree")
    TFileData = ROOT.TFile(Datafile)
    TTreeData = TFileData.Get("tree")

    Punzis = []
    a = 2 #number of sigma
    reg1, reg2, reg3 = (0.19747546044624495, 0.6406002800264298, 0.16192425952732517)
    SB_factor = reg2/(reg1+reg3)
    #nData  = TTreeData.GetEntries()
    #nMC    = TTreeMC.GetEntries()
    #print(nData)
    #print(nMC)
    nData, nMC = 150000,150000
    cutlist = np.linspace(LC1,HC1,cuts1)
    cutlist2 = np.linspace(LC2,HC2,cuts2)
    for cut in cutlist:
        passedMCs = []
        passedDatas = []
        
        passedMC1   = 0
        passedData1 = 0
        if bkgFirst1 == True:
            passedMCEvents = []
            passedDataEvents = []
            for event in range(nData):
                TTreeData.GetEntry(event)
                if getattr(TTreeData,attr1) > cut:
                    passedDataEvents.append(event)
                    passedData1 += 1
            for event in range(nMC):
                truthID = getattr(TTreeMC, "BmumuKst_isTrueResBd")
                TTreeMC.GetEntry(event)
                if getattr(TTreeMC, attr1) > cut and truthID:
                    passedMCEvents.append(event)
                    passedMC1   += 1


            for cut2 in cutlist2:
                passedMC2 = 0
                passedData2 = 0
                for passed_event in passedDataEvents:
                    TTreeData.GetEntry(passed_event)
                    truthID = getattr(TTreeMC, "BmumuKst_isTrueResBd")
                    if bkgFirst2 == True:
                        if getattr(TTreeData,attr2) > cut2 and truthID:
                            passedData2 += 1
                    else:
                        if getattr(TTreeData,attr2) < cut2:
                            passedData2 += 1
                passedDatas.append(passedData1+passedData2)
            for cut2 in cutlist2:
                passedMC2 = 0
                passedData2 = 0
                for passed_event in passedMCEvents:
                    TTreeMC.GetEntry(passed_event)
                    if bkgFirst2 == True:
                        if getattr(TTreeMC,attr2) > cut2:
                            passedMC2 += 1
                    else:
                        if getattr(TTreeMC,attr2) < cut2:
                            passedMC2 += 1
                passedMCs.append(passedMC1+passedMC2)
            eps_cuts = np.array(passedMCs)/float(nMC)
            Punzi_sigs = eps_cuts/(a/2 + np.sqrt(SB_factor*np.array(passedDatas)))
            Punzis.append(Punzi_sigs)
        else: #-----------------------------Opposite cut scheme, code repeat with < changes-----------------
            passedMCEvents = []
            passedDataEvents = []
            for event in range(nData):
                TTreeData.GetEntry(event)
                if getattr(TTreeData,attr1) < cut:
                    passedDataEvents.append(event)
                    passedData1 += 1

            for event in range(nMC):
                TTreeMC.GetEntry(event)
                truthID = getattr(TTreeMC, "BmumuKst_isTrueResBd")
                if getattr(TTreeMC, attr1) < cut and truthID == True:
                    passedMCEvents.append(event)
                    passedMC1 += 1

            for cut2 in cutlist2:
                passedMC2 = 0
                passedData2 = 0
                for passed_event in passedDataEvents:
                    TTreeData.GetEntry(passed_event)
                    if bkgFirst2 == True:
                        if getattr(TTreeData,attr2) > cut2:
                            passedData2 += 1
                    else:
                        if getattr(TTreeData,attr2) < cut2:
                            passedData2 += 1
                passedDatas.append(passedData1+passedData2)
            for cut2 in cutlist2:
                passedMC2 = 0
                passedData2 = 0
                for passed_event in passedMCEvents:
                    TTreeMC.GetEntry(passed_event)
                    truthID = getattr(TTreeMC, "BmumuKst_isTrueResBd")
                    if bkgFirst2 == True:
                        if getattr(TTreeMC,attr2) > cut2 and truthID == True:
                            passedMC2 += 1
                    else:
                        if getattr(TTreeMC,attr2) < cut2:
                            passedMC2 += 1
                passedMCs.append(passedMC1+passedMC2)
            eps_cuts = np.array(passedMCs)/float(nMC)
            Punzi_sigs = eps_cuts/(a/2 + np.sqrt(passedDatas))
            Punzis.append(Punzi_sigs)
    return cutlist, cutlist2, Punzis               

#cossigcuts, taucuts, sigs = CutOptimiser("/afs/cern.ch/user/c/cklo/public/TestingMC.root","/afs/cern.ch/user/c/cklo/public/TestingData_Sideband_JPsi.root","BmumuKst_Lxy_over_err", "BmumuKst_CosTheta_err",True,True,1,0.000001,50,0.01,10,10)
cossigcuts, taucuts, sigs = CutOptimiser("/afs/cern.ch/user/c/cklo/public/TestingMC.root","/afs/cern.ch/user/c/cklo/public/TestingData_Sideband_JPsi.root","BmumuKst_CosTheta_err", "BmumuKst_Lxy_minA0",True,True,0.01,0.1,0.1,3.1,30,30)
#cossigcuts, taucuts, sigs = CutOptimiser("/afs/cern.ch/user/c/cklo/public/TestingMC.root","/afs/cern.ch/user/c/cklo/public/TestingData_Sideband_JPsi.root","BmumuKst_Lxy_minA0", "BmumuKst_CosTheta_err",True,True,0.1,0.01,2.1,0.21,10,10)
print(max([max(sig) for sig in sigs]))
max_theta_cut = np.argmax([max(sig) for sig in sigs])
print(cossigcuts[max_theta_cut], taucuts[np.argmax(sigs[max_theta_cut])])
plot = True
if plot == True:
    plt.figure(1)
    for i,signif in enumerate(sigs):
        if i != max_theta_cut:
            plt.plot(taucuts,signif, color='b')#)# ,label = 'Lxy Signif : {}'.format(cossigcuts[i]), color='b')
    plt.plot(taucuts, sigs[max_theta_cut],label = 'costheta Signif : {}'.format(cossigcuts[max_theta_cut]), color = 'r')
    plt.legend()    
    plt.ylabel("Punzi significance")
    plt.xlabel("BmumuKst_Lxy_minA0")
    #plt.show()
    plt.savefig("Optimised_cuts_coolerplot.png")
